/**
 * This is a mini-pascal scanner that scans through
 * a mini-pascal file and returns tokens based on keywords and symbols.
 * @author Chase Strickler
 */

/* Declarations */
package scanner;

import java.util.HashMap;
import java.util.Map;
import java.lang.String;


%%

%public
%class  MyScanner   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */


%eofval{
  return null;
%eofval}

%{

  // Since zzAtEOF is private, I declared an accessor function here.
  public boolean getEOF(){
    return zzAtEOF;
  }

  Token answer = null;

  HashMap<String, TokenType> lookupTable = new HashMap<String, TokenType>();


  /**
   * Gets the line number of the most recent lexeme.
   * @return The current line number.
   */
  public int getLine() { return yyline;}
  
  /**
   * Gets the column number of the most recent lexeme.
   * This is the number of chars since the most recent newline char.
   * @return The current column number.
   */
  public int getColumn() { return yycolumn;}
  
%}

%init{

/* Keywords are added here that should return a token other than ID */
   lookupTable.put("program",     TokenType.PROGRAM);
   lookupTable.put("var",         TokenType.VAR);
   lookupTable.put("array",       TokenType.ARRAY);
   lookupTable.put("of",          TokenType.OF);
   lookupTable.put("or",          TokenType.OR);
   lookupTable.put("div",         TokenType.DIV);
   lookupTable.put("and",         TokenType.AND);
   lookupTable.put("integer",     TokenType.INTEGER);
   lookupTable.put("real",        TokenType.REAL);
   lookupTable.put("function",    TokenType.FUNCTION);
   lookupTable.put("procedure",   TokenType.PROCEDURE);
   lookupTable.put("begin",       TokenType.BEGIN);
   lookupTable.put("end",         TokenType.END);
   lookupTable.put("if",          TokenType.IF);
   lookupTable.put("then",        TokenType.THEN);
   lookupTable.put("else",        TokenType.ELSE);
   lookupTable.put("while",       TokenType.WHILE);
   lookupTable.put("do",          TokenType.DO);
   lookupTable.put("not",         TokenType.NOT);
   lookupTable.put("mod",         TokenType.MOD);
   lookupTable.put("read",        TokenType.READ);
   lookupTable.put("write",       TokenType.WRITE);
   lookupTable.put("return",      TokenType.RETURN);

%init}


/* Patterns */



/* Miscellaneous Regex */
other         = .
digit         = [0-9]
digits        = {digit}{digit}*
letter        = [A-Za-z]


/* Curly bracers included as comments are whitespace */
whitespace    = [\n|\t|\r|\{|\}| ]

/* Id is anything that starts with a letter followed by any number of letters or digits or underscores or dashes */
id            = {letter}({letter} | {digit} | _ | -)*
num           = [+-]?([0-9]+|[0-9]*\.[0-9]+([eE][-+]?[0-9]+)?)



/* Symbols Regex */
left_bracket  = (\[)
right_bracket = (\])
left_par      = (\()
right_par     = (\))
/* addop         = (\+|\-) */
plus          = (\+)
minus         = (\-)
assignop      = (\:)(\=)
relop         = (<(=|>)?|>=?|=)

/*May need to add individual symbols for relop */

/* mulop         = (\*|\/) */
mult          = (\*)
div           = (\/)
colon         = (\:)
semicolon     = (\;)
comma         = (\,)
period        = (\.)
  


%%
/* Lexical Rules */

/* Symbols Rules Begin */
{assignop}  {
            answer = new Token(yytext(), TokenType.ASSIGNOP);
            return answer;
}

/*
{addop}  {
            answer = new Token(yytext(), TokenType.ADDOP);
            return answer;
}
*/

{plus}  {
            answer = new Token(yytext(), TokenType.PLUS);
            return answer;
}

{minus}  {
            answer = new Token(yytext(), TokenType.MINUS);
            return answer;
}

{relop}  {
            answer = new Token(yytext(), TokenType.RELOP);
            return answer;

}

{left_par}  {
            answer = new Token(yytext(), TokenType.LEFTPAR);
            return answer;
}

{right_par}  {
            answer = new Token(yytext(), TokenType.RIGHTPAR);
            return answer;
}

{left_bracket}  {
            answer = new Token(yytext(), TokenType.LEFTBRKT);
            return answer;
}

{right_bracket}  {
            answer = new Token(yytext(), TokenType.RIGHTBRKT);
            return answer;
}

{comma}  {
            answer = new Token(yytext(), TokenType.COMMA);
            return answer;
}

{period}  {
            answer = new Token(yytext(), TokenType.PERIOD);
            return answer;
}

{colon}  {
            answer = new Token(yytext(), TokenType.COLON);
            return answer;
}

{semicolon}  {
            answer = new Token(yytext(), TokenType.SEMICOLON);
            return answer;
}

/*
{mulop}     {
            answer = new Token(yytext(), TokenType.MULOP);
            return answer;
}
*/

{mult}     {
            answer = new Token(yytext(), TokenType.MULTIPLY);
            return answer;
}

{div}     {
            answer = new Token(yytext(), TokenType.DIVIDE);
            return answer;
}

/* Keywords Rules End */

/* Miscellaneous Rules Begin */
{id}  {
  // Id tag checks the lookup table to see if the keyword is in it. If not, return ID, else, return the token
            if (lookupTable.containsKey(yytext()) == false){
              answer = new Token(yytext(), TokenType.ID);
              return answer;
            }
            else{
              answer = new Token(yytext(), lookupTable.get(yytext()));
              return answer;
            }
}

{num} {
              answer = new Token(yytext(), TokenType.NUM);
              return answer;
}

{whitespace}+ {

}

/* Keywords Rules End */

/* Miscellaneous Rules Begin */


{other}    {
             answer = new Token(yytext(), TokenType.ERROR);
             return answer;
           }

/* Miscellaneous Rules End */
