
# Writing code for ProgramNode
.data
global_space:  .asciiz " " 
global_endl:   .asciiz "\n" 
max_num1: .word 0
max_num2: .word 0
setAnswer_num: .word 0

# Writing code for DeclarationsNode
global_answer: .word 0
# ----------- End DeclarationNode ------------


.text
main:
addi $sp, $sp, -4
sw $ra, 0($sp)

# Writing code for CompoundStatementNode

# Writing Code for AssignmentStatementNode

# Writing Code for ValueNode
addi   $t0,   $zero, 3
# ----------- End ValueNode ------------

# Writing Code for VariableNode
lw $t1, global_answer
# ----------- End VariableNode ------------
sw $t0, global_answer
# ----------- End AssignmentStatementNode ------------

# Writing Code for AssignmentStatementNode

# Writing code for OperationNode

# Writing Code for VariableNode
lw $t2, global_answer
# ----------- End VariableNode ------------

# Writing Code for FunctionCallNode

# Writing Code for VariableNode
lw $a0, global_answer
# ----------- End VariableNode ------------

# Writing Code for ValueNode
addi   $a1,   $zero, 4
# ----------- End ValueNode ------------
jal max
move $t3 $v0
add    $t0,   $t2,   $t3
# ----------- End OperationNode ------------

# Writing Code for VariableNode
lw $t1, global_answer
# ----------- End VariableNode ------------
sw $t0, global_answer
# ----------- End AssignmentStatementNode ------------

# Writing Code for WriteStatementNode

# Writing Code for VariableNode
lw $t0, global_answer
# ----------- End VariableNode ------------
li $v0, 1 
add $a0, $t0, $zero
syscall
# Prints a line feed
addi $a0, $0, 0xA #ascii code for LF 
addi $v0, $0, 0xB #syscall 11 prints lower 8 bits of $a0
syscall
# ----------- End WriteStatementNode ------------

# Writing Code for ProcedureCallNode

# Writing Code for ValueNode
addi   $a0,   $zero, 91
# ----------- End ValueNode ------------
jal setAnswer
# ----------- End ProcedureCallNode ------------

# Writing Code for WriteStatementNode

# Writing Code for VariableNode
lw $t0, global_answer
# ----------- End VariableNode ------------
li $v0, 1 
add $a0, $t0, $zero
syscall
# Prints a line feed
addi $a0, $0, 0xA #ascii code for LF 
addi $v0, $0, 0xB #syscall 11 prints lower 8 bits of $a0
syscall
# ----------- End WriteStatementNode ------------
# ----------- End CompoundStatementNode ------------
lw $ra, 0($sp)
addi $sp, $sp, 4
jr $ra

# ----------- End ProgramNode ------------
max:
sw $a0 max_num1
sw $a1 max_num2

# Writing code for CompoundStatementNode

# Writing Code for IfStatementNode

# Writing code for OperationNode

# Writing Code for VariableNode
lw $s0, max_num1
# ----------- End VariableNode ------------

# Writing Code for VariableNode
lw $s1, max_num2
# ----------- End VariableNode ------------
slt    $t0,    $s1,   $s0
# ----------- End OperationNode ------------
beqz   $t0, Else0
Then0:

# Writing Code for ReturnStatementNode

# Writing Code for VariableNode
lw $v0, max_num1
# ----------- End VariableNode ------------
j EndIf0
Else0:

# Writing Code for ReturnStatementNode

# Writing Code for VariableNode
lw $v0, max_num2
# ----------- End VariableNode ------------
EndIf0:

# ----------- End IfStatementNode ------------
# ----------- End CompoundStatementNode ------------
jr $ra
setAnswer:
sw $a0 setAnswer_num

# Writing code for CompoundStatementNode

# Writing Code for AssignmentStatementNode

# Writing Code for VariableNode
lw $t0, setAnswer_num
# ----------- End VariableNode ------------

# Writing Code for VariableNode
lw $t1, global_answer
# ----------- End VariableNode ------------
sw $t0, global_answer
# ----------- End AssignmentStatementNode ------------
# ----------- End CompoundStatementNode ------------
jr $ra
