/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scanner;

import java.io.StringReader;
import java.io.Reader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stric
 */
public class MyScannerTest {
    
    public MyScannerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of nextToken method, of class MyScanner.
     */
    @Test
    public void testNextToken() throws Exception {
        System.out.println("nextToken() Test...");
        
        // This is a string of micro pascal code to be tested.
        // This is testing the keywords of the language.
        String str = ("program var array of or div "
                + "and integer real function procedure "
                + "begin end if then else while do not "
                + "mod and read write return");
        StringReader input = new StringReader(str);
        MyScanner scanner = new MyScanner(input);
        
        /* Begin Keywords Test */
        
        // Program Token Test
        Token result = scanner.nextToken();
        assertEquals(TokenType.PROGRAM, result.type);
        
        // Var Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.VAR, result.type);
        
        // Array Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.ARRAY, result.type);
        
        // Of Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.OF, result.type);
        
        // Or Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.OR, result.type);
        
        // Div Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.DIV, result.type);
        
        // And Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.AND, result.type);
        
        // Integer Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.INTEGER, result.type);
        
        // Real Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.REAL, result.type);
        
        // Function Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.FUNCTION, result.type);
        
        // Procedure Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.PROCEDURE, result.type);
        
        // Begin Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.BEGIN, result.type);
        
        // End Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.END, result.type);
        
        // If Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.IF, result.type);
        
        // Then Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.THEN, result.type);
        
        // Else Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.ELSE, result.type);
        
        // While Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.WHILE, result.type);
        
        // Do Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.DO, result.type);
        
        // Not Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.NOT, result.type);
        
        // Mod Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.MOD, result.type);
        
        // And Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.AND, result.type);
        
        // Read Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.READ, result.type);
        
        // Write Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.WRITE, result.type);
        
        // Return Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.RETURN, result.type);
//        
//        // Semicolon Token Test
//        result = scanner.nextToken();
//        assertEquals(TokenType.SEMICOLON, result.type);
//        
        /* End Keywords Test */
        
        
        /* Begin Numbers Test */
        
        str = ("3 01 2.34 3.3E+1 002831");
        input = new StringReader(str);
        scanner = new MyScanner(input);
        
        // 3 Num Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.NUM, result.type);
        
        // 01 Num Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.NUM, result.type);
        
        // 2.34 Num Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.NUM, result.type);
        
        // 3.3E+1 Num Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.NUM, result.type);
        
        // 002831 Num Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.NUM, result.type);
        
        /* End Numbers Test */
        
        /* Begin Miscellaneous Tests */
        
        str = ("[ ] foo b_ar asd9e23 * + - / = : := ; ,");
        input = new StringReader(str);
        scanner = new MyScanner(input);
        
        // Left Bracket Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.LEFTBRKT, result.type);
        
        // Right Bracket Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.RIGHTBRKT, result.type);
        
        // foo ID Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.ID, result.type);
        
        // b_ar ID Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.ID, result.type);
        
        // asd9e23 ID Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.ID, result.type);
        
        // Multiply Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.MULTIPLY, result.type);
        
        // + Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.PLUS, result.type);
        
        // - Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.MINUS, result.type);
        
        // / Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.DIVIDE, result.type);
        
        // = Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.RELOP, result.type);
        
        // : Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.COLON, result.type);
        
        // := Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.ASSIGNOP, result.type);
        
        // ; Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.SEMICOLON, result.type);
        
        // , Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.COMMA, result.type);
        
        
       /*
        * This should return an error, since it is an 
        * invalid ID and an invalid NUM, but yet it
        * returns a NUM. I need to fix this REGEX but
        * can't seem to get it working now.
        
        // 23DADSF Error Token Test
        result = scanner.nextToken();
        assertEquals(TokenType.ERROR, result.type);
       
        */
        
        /* End Miscellaneous Tests */
 
    }
    
}
