package semanticanalyzer;

import org.junit.Test;
import parser.Parser;
import syntaxtree.ProgramNode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SemanticAnalyzerTest {

    /**
     * This function is used to check the assignTypes function. Only one test is used as this test shows that the
     * function is capable of adding both types to all nodes.
     */
    @Test
    public void assignTypes() {
        System.out.println("Test of assignTypes() Function: ");
        String pascal =
                "program sample;\n" +
                "var dollars, yen, bitcoins: integer;\n" +
                "var something: real;\n" +
                "\n" +
                "begin\n" +
                "  dollars := 1000000 + something;\n" +
                "  yen := dollars * 110;\n" +
                "  bitcoins := dollars / 3900\n" +
                "end.";
        Parser parser = new Parser(pascal, false);
        ProgramNode actual = parser.program();
        SemanticAnalyzer analyzer = new SemanticAnalyzer(actual);
        System.out.println("Before type assignment: \n");
        String before = actual.indentedToString(0);
        System.out.println(before);
        analyzer.assignTypes(actual.getMain());
        System.out.println("\nAfter type assignment: \n");
        String after = actual.indentedToString(0);
        System.out.println(after);

        String expected = "Program: sample\n" +
                "|-- Declarations\n" +
                "|-- --- Name: dollars, Type: INTEGER\n" +
                "|-- --- Name: yen, Type: INTEGER\n" +
                "|-- --- Name: bitcoins, Type: INTEGER\n" +
                "|-- SubProgramDeclarations\n" +
                "|-- CompoundStatement\n" +
                "|-- --- Assignment\n" +
                "|-- --- --- Name: dollars, Type: INTEGER\n" +
                "|-- --- --- Operation: PLUS, Type REAL\n" +
                "|-- --- --- --- Value: 1000000, Type INTEGER\n" +
                "|-- --- --- --- Name: something, Type: REAL\n" +
                "|-- --- Assignment\n" +
                "|-- --- --- Name: yen, Type: INTEGER\n" +
                "|-- --- --- Operation: MULTIPLY, Type INTEGER\n" +
                "|-- --- --- --- Name: dollars, Type: INTEGER\n" +
                "|-- --- --- --- Value: 110, Type INTEGER\n" +
                "|-- --- Assignment\n" +
                "|-- --- --- Name: bitcoins, Type: INTEGER\n" +
                "|-- --- --- Operation: DIVIDE, Type INTEGER\n" +
                "|-- --- --- --- Name: dollars, Type: INTEGER\n" +
                "|-- --- --- --- Value: 3900, Type INTEGER\n";
        System.out.println("\nExpected: \n" + expected);
        assertEquals(expected, after);
    }

    /**
     * This function check the types across assignments. Three checks are done within this function, one to
     * ensure that errors are caught, another to ensure that correct code passes, and a third edge case to check
     * that REAL types can be assigned INTEGERs.
     */
    @Test
    public void checkTypes() {
        //Tests that an error is caught on assigning something, a real, to dollars, an integer
        System.out.println("Test #1 of checkTypes() Function: \n");
        String pascal =
                "program sample;\n" +
                        "var dollars, yen, bitcoins: integer;\n" +
                        "var something: real;\n" +
                        "\n" +
                        "begin\n" +
                        "  dollars := 1000000 + something;\n" +
                        "  yen := dollars * 110;\n" +
                        "  bitcoins := dollars / 3900\n" +
                        "end.";
        System.out.println("Input: \n" + pascal +"\n");
        Parser parser = new Parser(pascal, false);
        ProgramNode actual = parser.program();

        SemanticAnalyzer analyzer = new SemanticAnalyzer(actual);
        analyzer.assignTypes(actual.getMain());
        System.out.println("Actual: \n");
        analyzer.checkTypes(actual.getMain());

        String expected = "Error, dollars is of type INTEGER and is trying to be assigned PLUS of type REAL.\n";
        System.out.println("\nExpected: \n" + expected);

        //Tests that no errors are thrown on semantically correct code
        System.out.println("Test #2 of checkTypes() Function: \n");
        pascal =
                "program sample;\n" +
                        "var dollars, yen, bitcoins: integer;\n" +
                        "var something: real;\n" +
                        "\n" +
                        "begin\n" +
                        "  dollars := 1000000;\n" +
                        "  yen := dollars * 110;\n" +
                        "  bitcoins := dollars / 3900\n" +
                        "end.";
        System.out.println("Input: \n" + pascal +"\n");
        System.out.println("Actual: \n");
        parser = new Parser(pascal, false);
        actual = parser.program();
        analyzer = new SemanticAnalyzer(actual);
        analyzer.assignTypes(actual.getMain());
        analyzer.checkTypes(actual.getMain());
        System.out.println("Expected (nothing): \n");

        //Tests that real types can be assigned integers
        System.out.println("Test #3 of checkTypes() Function: \n");
        pascal =
                "program sample;\n" +
                        "var dollars, yen, bitcoins: integer;\n" +
                        "var something: real;\n" +
                        "\n" +
                        "begin\n" +
                        "  dollars := 1000000;\n" +
                        "  something := dollars * 110;\n" +
                        "  bitcoins := dollars / 3900\n" +
                        "end.";
        System.out.println("Input: \n" + pascal +"\n");
        System.out.println("Actual: \n");
        parser = new Parser(pascal, false);
        actual = parser.program();
        analyzer = new SemanticAnalyzer(actual);
        analyzer.assignTypes(actual.getMain());
        analyzer.checkTypes(actual.getMain());
        System.out.println("Expected (nothing): \n");
    }

    /**
     * Tests that the checkVariableDecs function of the analyzer works properly. This is done by passing it
     * correct code and ensuring that it passes, then by passing it incorrect code and making sure it errors.
     */
    @Test
    public void checkVariableDecs() {
        //Tests that an error is caught on assigning something, a real, to dollars, an integer
        System.out.println("Test #1 of checkVariableDecs() Function: \n");
        String pascal =
                "program sample;\n" +
                        "var dollars, yen, bitcoins: integer;\n" +
                        "\n" +
                        "begin\n" +
                        "  dollars := 1000000;\n" +
                        "  yen := dollars * 110;\n" +
                        "  bitcoins := dollars / 3900\n" +
                        "end.";
        System.out.println("Input: \n" + pascal +"\n");
        Parser parser = new Parser(pascal, false);
        ProgramNode actual = parser.program();

        SemanticAnalyzer analyzer = new SemanticAnalyzer(actual);
        analyzer.assignTypes(actual.getMain());
        System.out.println("Actual: \n");
        parser = new Parser(pascal, false);
        actual = parser.program();
        analyzer = new SemanticAnalyzer(actual);
        analyzer.assignTypes(actual.getMain());
        analyzer.checkTypes(actual.getMain());
        analyzer.checkVariableDecs(actual.getMain());
        System.out.println("Expected: \n");

        //Tests that no errors are thrown on semantically correct code
        System.out.println("Test #2 of checkVariableDecs() Function: \n");
        pascal =
                "program sample;\n" +
                "var dollars, yen, bitcoins: integer;\n" +
                "\n" +
                "begin\n" +
                "  dollars := 1000000 + something;\n" +
                "  yen := dollars * 110;\n" +
                "  bitcoins := dollars / 3900\n" +
                "end.";
        System.out.println("Input: \n" + pascal +"\n");
        System.out.println("Actual: \n");
        parser = new Parser(pascal, false);
        try {
            actual = parser.program();
            analyzer = new SemanticAnalyzer(actual);
            analyzer.assignTypes(actual.getMain());
            analyzer.checkTypes(actual.getMain());
            fail("Reached unreachable code");
        }
        catch (Exception e){
            assertEquals(null, e.getMessage());
            System.out.println(e + "\n");
            System.out.println("Expected: \njava.lang.NullPointerException");
        }
    }
}