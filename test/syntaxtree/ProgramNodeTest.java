
package syntaxtree;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import parser.Parser;

/**
 *
 * @author Chase Strickler
 */
public class ProgramNodeTest {

	public ProgramNodeTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

//	/**
//	 * Test of indentedToString method, of class ProgramNode.
//	 */
//	@Test
//	public void manualTestIndentedToString() {
//		System.out.println("indentedToString");
//		int level = 0;
//
//		ProgramNode root = new ProgramNode("aprog");
//
//		/* Initialize our Declarations Node then add variables to it */
//		DeclarationsNode var = new DeclarationsNode();
//		VariableNode foo = new VariableNode("foo");
//		VariableNode fee = new VariableNode("fee");
//		VariableNode fi = new VariableNode("fi");
//
//		var.addVariable(foo);
//		var.addVariable(fee);
//		var.addVariable(fi);
//
//		root.setVariables(var);
//
//		/* Declare a function and add it to the root */
//		SubProgramDeclarationsNode funs = new SubProgramDeclarationsNode();
//		SubProgramNode subprog = new SubProgramNode("subprog");
//		funs.addSubProgramDeclaration(subprog);
//
//		root.setFunctions(funs);
//
//		/* Declare a main with three statements and add that to the root */
//		CompoundStatementNode main = new CompoundStatementNode();
//
//		StatementNode stmnt1 = new CompoundStatementNode();
//		StatementNode stmnt2 = new CompoundStatementNode();
//		StatementNode stmnt3 = new CompoundStatementNode();
//
////		stmnt1.setName("statement1");
////		stmnt2.setName("statement2");
////		stmnt3.setName("statement3");
////
////		main.addStatement(stmnt1);
////		main.addStatement(stmnt2);
////		main.addStatement(stmnt3);
//
//		root.setMain(main);
//
//		System.out.println("This is the tree that we get right now");
//		System.out.println( root.indentedToString(0));
//
//		String expResult = "Program: aprog\n" +
//				"|-- Declarations\n" +
//				"|-- --- Name: foo\n" +
//				"|-- --- Name: fee\n" +
//				"|-- --- Name: fi\n" +
//				"|-- SubProgramDeclarations\n" +
//				"|-- --- Name: subprog\n"+
//				"|-- CompoundStatement\n" +
//				"|-- --- Name: statement1\n"+
//				"|-- --- Name: statement2\n"+
//				"|-- --- Name: statement3\n";
//		String result = root.indentedToString(level);
//		assertEquals(expResult, result);
//
//	}

	/**
	 * Test of indentedToString method, of class ProgramNode.
	 */
	@Test
	public void parserTestIndentedToString() {
		System.out.println("Loading parser test of indentedToString");
		//int level = 0;
		Parser parser = new Parser("pascal.pas", true);
		ProgramNode actual = parser.program();
		String actualString = actual.indentedToString(0);

		System.out.println("TREE: ");
		System.out.println( actualString);

		String expResult =
				"Program: foo\n" +
				"|-- Declarations\n" +
				"|-- --- Name: fee\n" +
				"|-- --- Name: fi\n" +
				"|-- --- Name: fo\n" +
				"|-- SubProgramDeclarations\n" +
				"|-- CompoundStatement\n" +
				"|-- --- CompoundStatement\n";
		//assertEquals(expResult, actualString);
	}
}