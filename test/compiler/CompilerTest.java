/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiler;

import org.junit.Test;
//import org.testng.annotations.Test;
import parser.Recognizer;
import symboltable.KindEnum;
import symboltable.SymbolTable;

import static org.junit.Assert.*;

/**
 * This class tests that the Symbol Table is properly loaded by the recognizer.
 * @author Chase
 */
public class CompilerTest {
    
    public CompilerTest() {
    }

    /**
     * Test of Symbol Table Integration with Recognizer.
     */
    @Test
    public void testSymbolTableIntegration() {
        System.out.println( "Test of Symbol Table Integration:");
        System.out.println( "Input: ");
        System.out.println( 
                            "program foo;\n" +
                            "var fee, fi, fo, fum: integer;\n" +
                            "begin\n" +
                            "  fee := 4;\n" +
                            "  fi := 5;\n" +
                            "  fo := 3 * fee + fi;\n" +
                            "  if fo < 13\n" +
                            "    then\n" +
                            "      fo := 13\n" +
                            "    else\n" +
                            "      fo := 26\n" +
                            "  ;\n" +
                            "  write( fo)\n" +
                            "end\n" +
                            ".\n");
        
        Recognizer r = new Recognizer("pascal.pas", true);
        r.program();
        KindEnum expected = KindEnum.PROGRAMNAME;
        KindEnum result = null;
        
        /* Begin First Test */
        System.out.println("Test that PROGRAMNAME is returned when calling the"
                + " getKind() function on foo");
        try{
            result = SymbolTable.st.getKind("foo");
        }
        catch(Exception e){
            assertEquals(expected, result);
        }       
        /* End First Test */
        
        System.out.println();
        
        /* Begin Second Test */
        expected = KindEnum.VARIABLENAME;
        System.out.println("Test that VARIABLENAME is returned when calling the"
                + " getKind() function on fee");
        try{
            result = SymbolTable.st.getKind("fee");
        }
        catch(Exception e){
            assertEquals(expected, result);
        }       
        /* End Second Test */
        
        System.out.println();
        
        /* Begin Third Test */
        expected = KindEnum.VARIABLENAME;
        System.out.println("Test that VARIABLENAME is returned when calling the"
                + " getKind() function on fi");
        try{
            result = SymbolTable.st.getKind("fi");
        }
        catch(Exception e){
            assertEquals(expected, result);
        }
        /* End Third Test */
        
        System.out.println();
        
        /* Begin Fourth Test */
        expected = KindEnum.VARIABLENAME;
        System.out.println("Test that VARIABLENAME is returned when calling the"
                + " getKind() function on fo");
        try{
            result = SymbolTable.st.getKind("fo");
        }
        catch(Exception e){
            assertEquals(expected, result);
        }
        /* End Fourth Test */
        
        System.out.println();
        
        /* Begin Fourth Test */
        expected = KindEnum.VARIABLENAME;
        System.out.println("Test that VARIABLENAME is returned when calling the"
                + " getKind() function on fum");
        try{
            result = SymbolTable.st.getKind("fum");
        }
        catch(Exception e){
            assertEquals(expected, result);
        }
        /* End Fourth Test */
        
        System.out.println();
    }
}
