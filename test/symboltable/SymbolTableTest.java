package symboltable;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * These are the tests of the functions in the symbol table. Each of these 
 * two tests use the addSymbol() functions to get the result, so I'm also testing
 * that function implicitly.
 * @author Chase Strickler
 */
public class SymbolTableTest {
    
    /**
     * Test of getKind method, of class SymbolTable.
     */
    @Test
    public void testGetKind() {
        System.out.println("getKind on ident: foo, kind: program");
        String ident = "foo";
        SymbolTable ST = new SymbolTable();
        ST.addSymbol(ident, KindEnum.PROGRAMNAME);
        KindEnum expResult = KindEnum.PROGRAMNAME;
        KindEnum result = ST.getKind(ident);
        assertEquals(expResult, result);
    }

    /**
     * Test of getType method, of class SymbolTable.
     */
    @Test
    public void testGetType() {
        System.out.println("getType on ident: bar, kind: variable, type: integer");
        String ident = "foo";
        SymbolTable ST = new SymbolTable();
        ST.addSymbol(ident, KindEnum.VARIABLENAME, DataType.INTEGER, "global");
        DataType expResult = DataType.INTEGER;
        DataType result = ST.getType(ident);
        assertEquals(expResult, result);
    }
}