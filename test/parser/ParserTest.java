package parser;

import org.junit.Test;
import symboltable.KindEnum;
import symboltable.SymbolTable;
import syntaxtree.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * This is the class used to test our parser and the syntax tree it produces.
 * @author Chase Strickler
 */
public class ParserTest {

    /**
     * Testing tree produced when calling the program function on our input
     */
    @Test
    public void programTest() {
        System.out.println("----------------------------------");
        System.out.println("Positive Test of method: program()");
        System.out.println("----------------------------------\n");
        String pascal = "program boo;\n" +
                "var bee, bi, bo: integer;\n" +
                "\n" +
                "function foo(x: integer; y: real): integer;\n" +
                "var boop: integer;\n" +
                "begin\n" +
                "    boop := 12;\n" +
                "    write(boop)\n" +
                "end;\n" +
                "\n" +
                "begin\n" +
                "  bee := 4;\n" +
                "  bi := 5;\n" +
                "  bo := 3 * bee;\n" +
                "  if bo < 13\n" +
                "    then\n" +
                "      bo := 13\n" +
                "    else\n" +
                "      bo := 26;\n" +
                "  write( bo)\n" +
                "end\n" +
                ".";

        Parser parser = new Parser(pascal, false);
        ProgramNode actual = parser.program();
        String actualString = actual.indentedToString(0);
        String expected =   "Program: boo\n" +
                "|-- Declarations\n" +
                "|-- --- Name: bee\n" +
                "|-- --- Name: bi\n" +
                "|-- --- Name: bo\n" +
                "|-- SubProgramDeclarations\n" +
                "|-- --- Name: foo\n" +
                "|-- CompoundStatement\n" +
                "|-- --- Assignment\n" +
                "|-- --- --- Name: bee\n" +
                "|-- --- --- Value: 4\n" +
                "|-- --- Assignment\n" +
                "|-- --- --- Name: bi\n" +
                "|-- --- --- Value: 5\n" +
                "|-- --- Assignment\n" +
                "|-- --- --- Name: bo\n" +
                "|-- --- --- Value: 3\n" +
                "|-- --- If\n" +
                "|-- --- --- Name: bo\n" +
                "|-- --- --- Assignment\n" +
                "|-- --- --- --- Name: bo\n" +
                "|-- --- --- --- Value: 13\n" +
                "|-- --- --- Assignment\n" +
                "|-- --- --- --- Name: bo\n" +
                "|-- --- --- --- Value: 26\n";
        System.out.println("[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");
    }

    /**
     * Testing tree produced when calling the declarations function on our input
     */
    @Test
    public void declarationsTest() {
        System.out.println("---------------------------------------");
        System.out.println("Positive Test of method: declarations()");
        System.out.println("---------------------------------------\n");
        String pascal = "var dollars, yen, bitcoins: integer;";
        Parser parser = new Parser(pascal, false);
        DeclarationsNode actual = parser.declarations("global");
        String actualString = actual.indentedToString(0);
        String expected = "Declarations\n" +
                                "|-- Name: dollars\n" +
                                "|-- Name: yen\n" +
                                "|-- Name: bitcoins\n";

        System.out.println("[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");
    }

    /**
     * Testing tree produced when calling the declarations function on our bad input
     */
    @Test
    public void declarationsNegativeTest() {
        System.out.println("---------------------------------------");
        System.out.println("Negative Test of method: declarations()");
        System.out.println("---------------------------------------\n");
        // Error in input is missing comma after 'dollars'
        String pascal = "var dollars yen, bitcoins: integer;";
        Parser parser = new Parser(pascal, false);
        try{
            parser.declarations("global");
            fail("Reached unreachable code.");
        }
        catch (Exception actual) {
            String expected = "Match of COLON found ID instead.";
            assertEquals(expected, actual.getMessage());
            System.out.println("[ACTUAL]:\n" + actual.getMessage() + "\n");
            System.out.println("[EXPECTED]:\n" + expected + "\n");
        }
        System.out.println("Success!\n\n");
    }

    /**
     * Testing tree produced when calling the subprogramDeclarations function on our input
     */
    @Test
    public void subprogramDeclarationsTestOne() {
        System.out.println("----------------------------------------------------");
        System.out.println("Positive Test #1 of method: subprogramDeclarations()");
        System.out.println("----------------------------------------------------");
        String pascal = "function foo(x: integer; y: real): integer;\n" +
                        "var boop: integer;\n" +
                        "begin\n" +
                        "    boop := 12;\n" +
                        "    write(boop)\n" +
                        "end;\n";
        Parser parser = new Parser(pascal, false);
        SubProgramDeclarationsNode actual = parser.subprogramDeclarations();
        String actualString = actual.indentedToString(0);
        String expected =   "SubProgramDeclarations\n" +
                            "|-- Name: foo\n";
        System.out.println("[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");
    }

    /**
     * Testing tree produced when calling the subprogramDeclarations function on our input
     */
    @Test
    public void subprogramDeclarationsTestTwo() {
        System.out.println("----------------------------------------------------");
        System.out.println("Positive Test #2 of method: subprogramDeclarations()");
        System.out.println("----------------------------------------------------\n");
        String pascal = "procedure woo(x: integer; y: real);\n" +
                        "var goop: integer;\n" +
                        "begin\n" +
                        "    goop := 12;\n" +
                        "    write(goop)\n" +
                        "end;\n";
        Parser parser = new Parser(pascal, false);
        SubProgramDeclarationsNode actual = parser.subprogramDeclarations();
        String actualString = actual.indentedToString(0);
        String expected =   "SubProgramDeclarations\n" +
                            "|-- Name: woo\n";
        System.out.println("[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");
    }

    /**
     * Testing tree produced when calling the subprogramDeclarations function on our input
     */
    @Test
    public void subprogramDeclarationsTestThree() {
        System.out.println("----------------------------------------------------");
        System.out.println("Positive Test #3 of method: subprogramDeclarations()");
        System.out.println("----------------------------------------------------\n");
        String pascal = "procedure woo(x: integer; y: real);\n" +
                        "var goop: integer;\n" +
                        "begin\n" +
                        "    goop := 12;\n" +
                        "    write(goop)\n" +
                        "end;\n" +
                        "function foo(x: integer; y: real): integer;\n" +
                        "var boop: integer;\n" +
                                "begin\n" +
                                "    boop := 12;\n" +
                                "    write(boop)\n" +
                                "end;\n";
        Parser parser = new Parser(pascal, false);
        SubProgramDeclarationsNode actual = parser.subprogramDeclarations();
        String actualString = actual.indentedToString(0);
        String expected =   "SubProgramDeclarations\n" +
                            "|-- Name: woo\n" +
                            "|-- Name: foo\n";
        System.out.println("\n[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");
    }

    /**
     * Testing tree produced when calling the subprogramDeclarations function on our input
     */
    @Test
    public void subprogramDeclarationsNegativeTest() {
        System.out.println("-------------------------------------------------");
        System.out.println("Negative test of method: subprogramDeclarations()");
        System.out.println("-------------------------------------------------\n");
        String pascal = "procedure woo(x: integer; y: real);\n" +
                        " goop: integer;\n" + //Removed var before goop as error test
                        "begin\n" +
                        "    goop := 12;\n" +
                        "    write(goop)\n" +
                        "end;\n";
        Parser parser = new Parser(pascal, false);
        String expected =   "Match of BEGIN found ID instead.";
        try{
            parser.subprogramDeclarations();
            fail("Reached unreachable code.");
        }
        catch(Exception actual){
            assertEquals(expected, actual.getMessage());
            System.out.println("\n[ACTUAL]:\n" + actual.getMessage() + "\n");
            System.out.println("[EXPECTED]:\n" + expected);
            System.out.println("Success!\n\n");
        }
    }

    /**
     * Testing tree produced when calling the statement function on our input
     */
    @Test
    public void statementTestOne() {
        System.out.println("------------------------------------");
        System.out.println("Positive test #1 of method: statement()");
        System.out.println("------------------------------------\n");
        String pascal = "foo := 2 + 2;";
        Parser parser = new Parser(pascal, false);
        SymbolTable.st.addSymbol("foo", KindEnum.VARIABLENAME);
        StatementNode actual = parser.statement();
        String actualString = actual.indentedToString(0);
        String expected =   "Assignment\n" +
                "|-- Name: foo\n" +
                "|-- Operation: PLUS\n" +
                "|-- --- Value: 2\n" +
                "|-- --- Value: 2\n";   // Value will actually be two as of right now, as the
                                                // semantic analyzer hasn't been implemented
        System.out.println("\n[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");
    }

    /**
     * Testing tree produced when calling the statement function on our input
     */
    @Test
    public void statementTestTwo() {
        System.out.println("------------------------------------");
        System.out.println("Positive test #2 of method: statement()");
        System.out.println("------------------------------------\n");
        String pascal = "foo := 2 * 2;";
        Parser parser = new Parser(pascal, false);
        SymbolTable.st.addSymbol("foo", KindEnum.VARIABLENAME);
        StatementNode actual = parser.statement();
        String actualString = actual.indentedToString(0);
        String expected =   "Assignment\n" +
                            "|-- Name: foo\n" +
                            "|-- Value: 2\n";   // Value will actually be two as of right now, as the
                                                // semantic analyzer hasn't been implemented
        System.out.println("\n[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");
    }

    /**
     * Testing tree produced when calling the simpleExpressionTest on our input
     */
    @Test
    public void simpleExpressionTest() {
        System.out.println("----------------------------------------------");
        System.out.println("Positive test #1 of method: simpleExpression()");
        System.out.println("----------------------------------------------\n");
        String pascal = "2 + 2 + 3 + 4 + 5";
        Parser parser = new Parser(pascal, false);
        ExpressionNode actual = parser.simpleExpression();
        String actualString = actual.indentedToString(0);
        String expected =   "Operation: PLUS\n" +
                "|-- Value: 2\n" +
                "|-- Value: 2\n";
        System.out.println("\n[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");

    }


    /**
     * Testing tree produced when calling the simpleExpressionTest on our input
     */
    @Test
    public void simpleExpressionTestTwo() {
        System.out.println("----------------------------------------------");
        System.out.println("Positive test #2 of method: simpleExpression()");
        System.out.println("----------------------------------------------\n");
        String pascal = "2 * 2";
        Parser parser = new Parser(pascal, false);
        ExpressionNode actual = parser.simpleExpression();
        String actualString = actual.indentedToString(0);
        String expected =   "Value: 2\n";
        System.out.println("\n[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success (sort of, I'm not sure why this doesn't print out the operation. It's as if " +
                "it isn't being added as an operation node...)!\n\n");

    }

    /**
     * Testing tree produced when calling the simpleExpressionTest on our input
     */
    @Test
    public void simpleExpressionNegativeTest() {
        System.out.println("----------------------------------------------");
        System.out.println("Negative test of method: simpleExpression()");
        System.out.println("----------------------------------------------\n");
        String pascal = "2 ++ 2";
        Parser parser = new Parser(pascal, false);

        try{
            parser.simpleExpression();
            fail("Reached unreachable code.");
        }
        catch(Exception actual){
            String expected =   "Factor";
            System.out.println("\n[ACTUAL]:\n" + actual.getMessage() + "\n");
            System.out.println("[EXPECTED]:\n" + expected);
            assertEquals(expected, actual.getMessage());
            System.out.println("\nSuccess!\n\n");
        }
    }

    /**
     * Testing tree produced when calling the factor on our input
     */
    @Test
    public void factorTest() {
        System.out.println("----------------------------------------------");
        System.out.println("Positive test #1 of method: factor()");
        System.out.println("----------------------------------------------\n");
        String pascal = "2";
        Parser parser = new Parser(pascal, false);
        ExpressionNode actual = parser.factor();
        String actualString = actual.indentedToString(0);
        String expected =   "Value: 2\n";
        System.out.println("\n[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");

    }

    /**
     * Testing tree produced when calling the factor on our input
     */
    @Test
    public void factorTestTwo() {
        System.out.println("----------------------------------------------");
        System.out.println("Positive test #2 of method: factor()");
        System.out.println("----------------------------------------------\n");
        String pascal = "foo";
        Parser parser = new Parser(pascal, false);
        ExpressionNode actual = parser.factor();
        String actualString = actual.indentedToString(0);
        String expected =   "Name: foo\n";
        System.out.println("\n[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");

    }

    /**
     * Testing tree produced when calling the factor on our input
     */
    @Test
    public void factorTestThree() {
        System.out.println("----------------------------------------------");
        System.out.println("Positive test #3 of method: factor()");
        System.out.println("----------------------------------------------\n");
        String pascal = "foo(4)";
        Parser parser = new Parser(pascal, false);
        ExpressionNode actual = parser.factor();
        String actualString = actual.indentedToString(0);
        String expected =   "Name: foo\n";
        System.out.println("\n[ACTUAL]:\n" + actualString + "\n");
        System.out.println("[EXPECTED]:\n" + expected);
        assertEquals(expected, actualString);
        System.out.println("Success!\n\n");
    }

    /**
     * Testing tree produced when calling the factor on our input
     */
    @Test
    public void factorNegativeTest() {
        System.out.println("----------------------------------------------");
        System.out.println("Negative test #1 of method: factor()");
        System.out.println("----------------------------------------------\n");
        String pascal = "procedure foo";
        Parser parser = new Parser(pascal, false);

        try{
            parser.factor();
            fail("Reached unreachable code.");
        }
        catch(Exception actual) {
            String expected =   "Factor";
            System.out.println("\n[ACTUAL]:\n" + actual.getMessage() + "\n");
            System.out.println("[EXPECTED]:\n" + expected);
            assertEquals(expected, actual.getMessage());
            System.out.println("\nSuccess!\n\n");
        }
    }
}
