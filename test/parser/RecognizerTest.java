package parser;

import org.junit.Test;
import static org.junit.Assert.*;


/**
 * This class performs JUnit testing on the Recognizer.java class.
 * The methods appended with BadPascal test the method on syntactically 
 * incorrect Pascal code. Some of the BadPascal compare via strings, and this
 * is because these functions fail in match().
 * @author Chase Strickler
 */
public class RecognizerTest {
    
    public RecognizerTest() {
    }

    /**
     * Test of program method, of class Recognizer.
     */
    @Test
    public void testProgram() {
        System.out.println("Program Test:");
        String s = 
"program foo;\n" +
"var fee, fi, fo, fum: integer;\n" +
"begin\n" +
"  fee := 4;\n" +
"  fi := 5;\n" +
"  fo := 3 * fee + fi;\n" +
"  if fo < 13\n" +
"    then\n" +
"      fo := 13\n" +
"    else\n" +
"      fo := 26\n" +
"  ;\n" +
"  write( fo)\n" +
"end\n" +
".";
        Recognizer r = new Recognizer(s, false);
        try{
            r.program();
        }
        catch(Exception e){
            fail("Program Test Failed.");
        }
        System.out.println();
    }
    
    /**
     * Test of program method, of class Recognizer, on bad input.
     */
    @Test
    public void testProgramBadPascal() {
        System.out.println("Program Test - Bad Pascal:");
        String s = "program foo begin end.";
        Recognizer r = new Recognizer(s, false);
        try{
            r.program();
            fail("Reached unreachable code.");
        }
        catch(Exception actual){
            String expected = "Match of SEMICOLON found BEGIN instead.";
            assertEquals(expected, actual.getMessage());
        }
        System.out.println();
    }

    /**
     * Test of identifierList method, of class Recognizer.
     */
    @Test
    public void testIdentifierList() {
        System.out.println("IdentifierList Test:");
        String s = "jack, jill, john";
        Recognizer r = new Recognizer(s, false);
        try{
            r.identifierList();
        }
        catch(Exception e){
            fail("IdentifierList Test Failed.");
        }
        System.out.println();
    }
    
    /**
     * Test of identifierList method, of class Recognizer on bad Pascal.
     */
   @Test
    public void testIdentifierListBadPascal() {
        System.out.println("IdentifierList Test - Bad Pascal:");
        String s = "jack 23";
        Recognizer r = new Recognizer(s, false);
        try{
            r.identifierList();
            
        }
        catch(Exception actual){
            Exception expected = new RuntimeException();
            assertEquals(expected, actual);
        }
        System.out.println();
    }

    /**
     * Test of declarations method, of class Recognizer.
     */
    @Test
    public void testDeclarations() {
        System.out.println("Declarations Test:");
        String s = "var foo : real ;";
        Recognizer r = new Recognizer(s, false);
        try{
            r.declarations("global");
        }
        catch(Exception e){
            fail("Declarations Test Failed.");
        }
        System.out.println();
    }
    
    /**
     * Test of declarations method, of class Recognizer on bad pascal.
     */
    @Test
    public void testDeclarationsBadPascal() {
        System.out.println("Declarations Test - Bad Pascal:");
        String s = "var foo : real ;";
        Recognizer r = new Recognizer(s, false);
        try{
            r.declarations("global");
            
        }
        catch(Exception actual){
            Exception expected = new RuntimeException();
            assertEquals(expected, actual);
        }
        System.out.println();
    }


    /**
     * Test of subprogramDec method, of class Recognizer.
     */
    @Test
    public void testSubprogramDec() {
       System.out.println("Subprogram Declaration Test:");
        String s = "procedure foo; begin end";
        Recognizer r = new Recognizer(s, false);
        try{
            r.subprogramDec();
        }
        catch(Exception actual){
            fail("Subprogram Test Failed");
        }
        System.out.println();
    }
    
    /**
     * Test of subprogramDec method, of class Recognizer on bad Pascal.
     */
    @Test
    public void testSubprogramDecBadPascal() {
       System.out.println("Subprogram Declaration Test - Bad Pascal:");
        String s = "procedure foo begin end";
        Recognizer r = new Recognizer(s, false);
        try{
            r.subprogramDec();
        }
        catch(Exception actual){
            String expected = "Match of SEMICOLON found BEGIN instead.";
            assertEquals(expected, actual.getMessage());
        }
        System.out.println();
    }

    /**
     * Test of subprogramHead method, of class Recognizer.
     */
    @Test
    public void testSubprogramHead() {
        System.out.println("Subprogram Head Test:");
        String s = "procedure foo;";
        Recognizer r = new Recognizer(s, false);
        try{
            r.subprogramHead();
        }
        catch(Exception actual){
            fail("SubprogramHead Test Failed");
        }
        System.out.println();
    }
    
    /**
     * Test of subprogramHead method, of class Recognizer on bad Pascal.
     */
    @Test
    public void testSubprogramHeadBadPascal() {
        System.out.println("Subprogram Head Test - Bad Pascal:");
        String s = "procedure foo";
        Recognizer r = new Recognizer(s, false);
        try{
            r.subprogramHead();
        }
        catch(Exception actual){
            String expected = "Match of SEMICOLON found null instead.";
            assertEquals(expected, actual.getMessage());
        }
        System.out.println();
    }

    /**
     * Test of statement method, of class Recognizer. Added cases to test 
     * ambiguity between variable and procedure calls
     */
    @Test
    public void testStatement() {
        System.out.println("Statement Test:");
        String s = "foo := 7";
        Recognizer r = new Recognizer(s, false);
        try{
            r.statement();
        }
        catch(Exception actual){
            fail("Statement Test Failed");
        }
        System.out.println();
        
        
        /* Test of statement -> variable call */
        s = "foo[12] := 7";
        Recognizer r2 = new Recognizer(s, false);
        try{
            r2.statement();
        }
        catch(Exception actual){
            fail("Statement Test Failed");
        }
        System.out.println();
        
        
        
        /* Test of statement -> procedure call */
        s = "fop (one, two, three)";
        Recognizer r3 = new Recognizer(s, false);
        try{
            r3.statement();
        }
        catch(Exception actual){
            fail("Statement Test Failed");
        }
        System.out.println();
    }
    
    /**
     * Test of statement method, of class Recognizer. on bad Pascal
     */
    @Test
    public void testStatementBadPascal() {
        System.out.println("Statement Test - Bad Pascal:");
        
        // First Statement Test
        String s = "foo :-= 7>";
        Recognizer r = new Recognizer(s, false);
        try{
            r.statement();
        }
        catch(Exception actual){
            Exception expected = new RuntimeException();
            assertEquals(expected, actual);
        }
        System.out.println();
        
        // Second Statement Test
        s = "2 foo";
        Recognizer r2 = new Recognizer(s, false);
        try{
            r2.statement();
            fail("Second Statement Test Failed");
        }
        catch(Exception actual){
            String expected = "Statement";
            assertEquals(expected, actual.getMessage());
        }
        System.out.println();
        
        // Third Statement Test
        s = "var foo ( 7>";
        Recognizer r3 = new Recognizer(s, false);
        try{
            r3.statement();
            fail("Third Statement Test Failed");
        }
        catch(Exception actual){
            String expected = "Statement";
            assertEquals(expected, actual.getMessage());
        }
        System.out.println();
    }

    /**
     * Test of simpleExpression method, of class Recognizer.
     */
    @Test
    public void testSimpleExpression() {
        System.out.println("SimpleExpression Test:");
        String s = "foo[7]";
        Recognizer r = new Recognizer(s, false);
        try{
            r.simpleExpression();
        }
        catch(Exception actual){
            fail("Simple Expression Test Failed");
        }
        System.out.println();
    }
    
     /**
     * Test of simpleExpression method, of class Recognizer on bad Pascal.
     */
    @Test
    public void testSimpleExpressionBadPascal() {
        System.out.println("SimpleExpression Test - Bad Pascal");
        
        // First Simple Expression Test
        String s = "foo[7] > bar";
        Recognizer r = new Recognizer(s, false);
        try{
            r.simpleExpression();
        }
        catch(Exception actual){
            Exception expected = new RuntimeException();
            assertEquals(expected, actual);
        }
        System.out.println();
        
        // Second Simple Expression Test
        s = "foo[7";
        Recognizer r2 = new Recognizer(s, false);
        try{
            r2.simpleExpression();
        }
        catch(Exception actual){
            String expected = "Match of RIGHTBRKT found null instead.";
            assertEquals(expected, actual.getMessage());
        }
        System.out.println();
    }

    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void testFactor() {
        System.out.println("Factor Test:");
        String s = "foo";
        Recognizer r = new Recognizer(s, false);
        try{
            r.factor();
        }
        catch(Exception actual){
            fail("Factor Test Failed");
        }
        System.out.println();
    }
    
     /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void testFactorBadPascal() {
        System.out.println("Factor Test - Bad Pascal");
        String s = "foo(7";
        Recognizer r = new Recognizer(s, false);
        try{
            r.factor();
        }
        catch(Exception actual){
            String expected = "Match of RIGHTPAR found null instead.";
            assertEquals(expected, actual.getMessage());
        }
        System.out.println();
    }
    
    
    
}
