# Extra: SubPrograms
## By: Chase Strickler

I've implemented subprograms into the compiler so that they are properly analyzed, compiled, and produced. 

#### Syntax Tree:
To achieve this, I
added the following nodes to the syntax tree: SubProgramHeadNode, FunctionCallNode, ProcedureCallNode, and a HasArguments interface. The SubProgramHeadNode 
holds the name and parameters of the subprogram. It is also used in constructing SubProgramNodes. The FunctionCallNode extends 
ExpressionNode, and is used to create a node when a function is called. Similarly, the ProcedureCallNode extends StatementNode and is used to create
a node when a procedure is called. These two nodes share an interface, HasArguments, to retrieve their arguments.

#### Parser:
Most of what I did to the parser was integrating the nodes created above and making sure that these were added to the symbol table. I also made 
sure to iterate across the declarations of each subprogram to add their respective variables with their scope.

#### Symbol Table:
I added a scope attribute to the symbols and added a constructor to add the scope. I also added accessor and mutator functions for the scope.

#### Semantic Analyzer:
I integrated the nodes so that they are properly analyzed for semantic correctness. I also integrated the nodes so
that types are assigned properly.

#### Code Generation:
Here, I created multiple functions to help create the subprograms. I created a function to iterate across each subprogram declaration and write code for 
it. I then added a function to write an individual subprogram root. To do this, I wrote the label/name of the function, then grabbed the
values from the arguments and loaded them into the proper parameters. From here, I wrote the main code of the function. Afterwards,
I jump returned to the $ra register. Writing the subprograms occurs after the programnode has written it's root.

Additionally, in the program node, I wrote all of the variables and parameters for each subprogram into the data section of the
program node. I also prepended each of these variables with the scope where it is declared. 

I also had to refactor the t register assignments. I now check the scope of the variable being declared, and if it isn't global,
I instead write it to an s register. This is necessary to avoid the overwriting of registers, since the function call might start at the 10th t register
but each subprogram must start at the 0th, since it is written after the program's main is written.

Function calls and procedure calls work very similarly. They simply call the label of the function and keep going. The only 
difference is that the function call moves whatever was in $v0 into the result register.