# Extra: Floating Points
## By: Chase Strickler

I've attempted to implement floating point numbers into the compiler. It currently is not working properly since I poorly wrote the assignment conversion checks. It
was also difficult to implement the floating point numbers after implementing the subprograms.

#### Code Generation:
What I did was create new variables to track the f registers, as well as use the s registers to track the saved floating point registers for functions and procedures. 
I then created another operations node function to write code for real numbers, using the real number arithmetic. Where I messed up was on the conversion and checking
when conversion is necessary. This isn't that hard of a problem, but since I started this feature late and poorly implemented it to start, I wasn't able to fix it.