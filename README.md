# Compiler Project
## By: Chase Strickler
### Description
This repository contains a compiler for the micro-pascal programming language. For instructions on how to run the project, see the User Manual in the product folder.

### JFlex Scanner
#### Added: 27 January 2019
A scanner has been added to this repository. You can run this by cloning the repository and opening it in NetBeans. To reproduce the scanner (MyScanner.java), navigate to src/scanner and type: "java -jar jflex.jar scannertest.jflex" into the command line.

### Recognizer
#### Added: 14 February 2019
A recognizer has been added to this repository. To run the recognizer, you need to instantiate it and point it at a file, then call the program() method.

### Symbol Table
#### Added: 24 February 2019
#### Updated: 3 March 2019
A symbol table has been added to this repository. It is linked with the Recognizer, and when symbols are found they are placed in the symbol table hash map.

### Syntax Tree
#### Added: 10 March 2019
A syntax tree has been added to the repository. The syntax tree is used to build nodes and acts as a way to traverse our program and analyze each node.

### Parser
#### Added: 4 April 2019
A parser has been added to this repository. The parser is a more robust recognizer and is used in conjunction with the syntax tree to build a tree.

### Semantic Analyzer
#### Added: 7 April 2019
A semantic analyzer has been added to the repository. The semantic analyzer determines whether the pascal passed is semantically correct.

### Code Generation
#### Added: 14 April 2019
Code generation has been added to the repository. The code gen generates assembly code from our pascal.

