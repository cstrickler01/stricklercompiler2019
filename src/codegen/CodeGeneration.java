package codegen;

import scanner.TokenType;
import symboltable.DataType;
import symboltable.SymbolTable;
import syntaxtree.*;

/**
 * This class will create code for an Equation tree.
 * @author Chase Strickler
 */
public class CodeGeneration {

    private int currentTRegister = 0;
    private int currentFRegister = 2;
    private int currentSRegister = 0;
    private int currentSkip = 0;
    private int currentLoop = 0;
    private int currentIf = 0;
    private ProgramNode root;
    private String scope = "global_";

    /**
     * Constructor to set the root of the tree as a program node.
     * @param node The root of the tree
     */
    public CodeGeneration(ProgramNode node){
        this.root = node;
    }

    /**
     * Begins writing code from the root of the tree, the program node, and outlines
     * the beginning of the assembly code.
     *
     * @return A String of assembly code.
     */
    public String writeCodeForRoot(){
        StringBuilder code = new StringBuilder();
        code.append("\n# Writing code for ProgramNode\n");
        code.append(".data\n");
        //code.append("answer: .word 0\n");
        code.append(scope + "space:  .asciiz \" \" \n");
        code.append(scope + "endl:   .asciiz \"\\n\" \n");
        SubProgramDeclarationsNode subprogs = root.getFunctions();
        for(SubProgramNode subprog : subprogs.getSubProgramNodes()){
            //scope = subprog.getHead().getName() + "_";

            for(VariableNode var : subprog.getHead().getArguments()){
                scope = SymbolTable.st.getScope(var.getName()) + "_";
                code.append(scope + var.getName() +": .word 0\n");
            }
            for(VariableNode var : subprog.getVariables().getVariables()){
                scope = SymbolTable.st.getScope(var.getName()) + "_";
                code.append(scope + var.getName() +": .word 0\n");
            }
        }

        String nodeCode = null;
        scope = "global_";
        nodeCode = writeCode(this.root.getVariables());
        code.append(nodeCode);
        code.append("\n\n");
        code.append(".text\n");
        code.append("main:\n");
        code.append("addi $sp, $sp, -4\n");
        code.append("sw $ra, 0($sp)\n");
        nodeCode = writeCode(this.root.getMain());
        code.append(nodeCode);
        code.append("lw $ra, 0($sp)\n");
        code.append("addi $sp, $sp, 4\n");
        code.append("jr $ra\n\n");
        code.append("# ----------- End ProgramNode ------------\n");
        code.append(writeCodeForSubRoots(root.getFunctions()));
        return code.toString();
    }

    /**
     * Function used to iterate through all subprogram declarations.
     * @param subprogs A subprogramdeclarations node.
     * @return A String of assembly code.
     */
    public String writeCodeForSubRoots(SubProgramDeclarationsNode subprogs){
        StringBuilder code = new StringBuilder();
        for(SubProgramNode subprog : subprogs.getSubProgramNodes()){
            code.append(writeCodeForSubRoot(subprog));
        }
        return code.toString();
    }

    /**
     * Function used to write the code for a sub program.
     * @param subprog The subprogram to write
     * @return A String of assembly.
     */
    public String writeCodeForSubRoot(SubProgramNode subprog){
        StringBuilder code = new StringBuilder();
        SubProgramHeadNode head = subprog.getHead();
        code.append(head.getName() + ":\n");
        //scope = head.getName() + "_";
        int argNumber = 0;
        for(VariableNode var : head.getArguments()){
            scope = SymbolTable.st.getScope(var.getName()) + "_";
            code.append("sw $a" + argNumber++ + " " + scope + var.getName() + "\n");
        }
        code.append(writeCode(subprog.getMain()));
        code.append("jr $ra\n");
        scope = "global_";
        return code.toString();
    }

    /**
     * Writes the code for the variable declarations.
     *
     * @param decNode The declaration node containing the variables of our pascal.
     * @return A String of assembly code.
     */
    public String writeCode(DeclarationsNode decNode){
        StringBuilder code = new StringBuilder();
        code.append("\n# Writing code for DeclarationsNode\n");
        for(VariableNode var : decNode.getVariables()){
            scope = SymbolTable.st.getScope(var.getName()) + "_";
            code.append(scope + var.getName() + ": .word 0\n");
        }
        code.append("# ----------- End DeclarationNode ------------\n");
        return code.toString();
    }

    /**
     * Writes code for a compound statement node
     * Loops through all statements in main and appends them to a string.
     * @param compNode The root of the main.
     * @return A String of assembly code
     */
    public String writeCode(CompoundStatementNode compNode){
        StringBuilder code = new StringBuilder();
        code.append("\n# Writing code for CompoundStatementNode\n");
        String nodeCode = null;
        for(StatementNode node : compNode.getStatements()){
            nodeCode = writeCode(node);
            code.append(nodeCode);
        }
        code.append("# ----------- End CompoundStatementNode ------------\n");
        return code.toString();
    }

    /**
     * This function determines what node the statementnode is and
     * routes it accodingly.
     * @param node The statement node to analyze
     * @return A String of assembly code
     */
    public String writeCode(StatementNode node){
        String nodeCode = null;
        if(node instanceof IfStatementNode){
            nodeCode = writeCode((IfStatementNode)node);
        }
        else if(node instanceof ReadStatementNode){
            nodeCode = writeCode((ReadStatementNode)node);
        }
        else if(node instanceof WhileStatementNode){
            nodeCode = writeCode((WhileStatementNode)node);
        }
        else if(node instanceof AssignmentStatementNode){
            nodeCode = writeCode((AssignmentStatementNode)node);
        }
        else if(node instanceof ReturnStatementNode){
            nodeCode = writeCode((ReturnStatementNode)node);
        }
        else if(node instanceof WriteStatementNode){
            nodeCode = writeCode((WriteStatementNode)node);
        }
        else if(node instanceof  CompoundStatementNode){
            nodeCode = writeCode((CompoundStatementNode)node);
        }
        else if(node instanceof  ProcedureCallNode){
            nodeCode = writeCode((ProcedureCallNode) node);
        }

        return nodeCode;
    }

    /**
     * Writes code for the given node.
     * This generic write code takes any ExpressionNode, and then
     * recasts according to subclass type for dispatching.
     * @param node The node for which to write code.
     * @param reg The register in which to put the result.
     * @return
     */
    public String writeCode( ExpressionNode node, String reg) {
        String nodeCode = null;
        if( node instanceof OperationNode) {
            nodeCode = writeCode((OperationNode)node, reg);
        }
        else if( node instanceof ValueNode) {
            nodeCode = writeCode((ValueNode)node, reg);
        }
        else if(node instanceof VariableNode) {
            nodeCode = writeCode((VariableNode)node, reg);
        }
        else if(node instanceof FunctionCallNode) {
            nodeCode = writeCode((FunctionCallNode)node, reg);
        }
        return( nodeCode);
    }

    /**
     * Dispatcher function for operations node
     * @param opNode
     * @param resultRegister
     * @return
     */
    public String writeCode( OperationNode opNode, String resultRegister){
        String nodeCode = null;
        if(opNode.getType() == DataType.INTEGER)
            nodeCode = writeIntOpCode(opNode, resultRegister);
        else if(opNode.getType() == DataType.REAL)
            nodeCode = writeIntOpCode(opNode, resultRegister);
        return nodeCode;
    }

    /**
     * Writes code for an operations node.
     * The code is written by gathering the child nodes' answers into
     * a pair of registers, and then executing the op on those registers,
     * placing the result in the given result register.
     * @param opNode The operation node to perform.
     * @param resultRegister The register in which to put the result.
     * @return The code which executes this operation.
     */
    public String writeIntOpCode( OperationNode opNode, String resultRegister)
    {
        String code = null;

        code = "\n# Writing code for OperationNode\n";
        ExpressionNode left = opNode.getLeft();
        String leftRegister = "";
        boolean isInteger = opNode.getLeft().getType() == DataType.INTEGER;
        int tRegs = 0;
        int sRegs = 0;
        int fRegs = 0;
        if(!(scope.equals("global_"))){
            leftRegister = "$s" + currentSRegister++;
            sRegs++;
        }
        else{
            leftRegister = "$t" + currentTRegister++;
            tRegs++;

        }
        code += writeCode( left, leftRegister);
        ExpressionNode right = opNode.getRight();
        String rightRegister = "";

        if(!(scope.equals("global_"))){
            rightRegister = "$s" + currentSRegister++;
            sRegs++;
            //System.out.println("T register increased! Value: " + currentTRegister);
        }
        else{
            rightRegister = "$t" + currentTRegister++;
            tRegs++;
            // System.out.println("T register increased! Value: " + currentTRegister);

        }
        code += writeCode( right, rightRegister);
        TokenType kindOfOp = opNode.getOperation();
        if( kindOfOp == TokenType.PLUS)
        {
            // add resultregister, left, right
            code += "add    " + resultRegister + ",   " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if( kindOfOp == TokenType.MINUS)
        {
            // subtract resultregister, left, right
            code += "sub    " + resultRegister + ",   " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if( kindOfOp == TokenType.MULTIPLY)
        {
            code += "mult   " + leftRegister + ",   " + rightRegister + "\n";
            code += "mflo   " + resultRegister + "\n";
        }
        if(kindOfOp == TokenType.DIVIDE){
            code += "div   " + leftRegister + ",   " + rightRegister + "\n";
            code += "mflo   " + resultRegister + "\n";
        }
        if(kindOfOp == TokenType.MOD){
            code += "div   " + leftRegister + ",   " + rightRegister + "\n";
            code += "mfhi   " + resultRegister + "\n";
        }

        if(kindOfOp == TokenType.OR){
            code += "or    " + resultRegister + ",   " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if(kindOfOp == TokenType.AND){
            code += "and    " + resultRegister + ",   " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if(kindOfOp == TokenType.LESS){
            code += "slt    " + resultRegister + ",    " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if(kindOfOp == TokenType.LESSEQ){
            code += "slt    " + resultRegister + ",    " + leftRegister +
                ",   " + rightRegister + "\n";
            code += "bne     " + leftRegister +
                    ",   " + rightRegister + ", Skip" + currentSkip + ":";
            code += "add     " + resultRegister + ",    $zero,    1";
            code += "Skip" + currentSkip + ":\" \n ";
        }
        if(kindOfOp == TokenType.GREAT){
            code += "slt    " + resultRegister + ",    " + rightRegister+
                    ",   " + leftRegister + "\n";
        }
        if(kindOfOp == TokenType.GREATEQ){
            code += "slt    " + resultRegister + ",    " + rightRegister +
                    ",   " + leftRegister + "\n";
            code += "bne     " + leftRegister +
                    ",   " + rightRegister + ", Skip" + currentSkip + "\n";
            code += "add     " + resultRegister + ",    $zero,    1\n";
            code += "Skip" + currentSkip + ": \n ";
        }
        if(kindOfOp == TokenType.EQUALS){
            code += "add    " + resultRegister + ",    $zero,   $zero\n";
            code += "bne    " + leftRegister + ",   " + rightRegister + ",   Skip" + currentSkip + "\n";
            code += "add     " + resultRegister + ",    $zero,    1\n";
            code += "Skip" + currentSkip + ": \n ";
        }
        currentTRegister = currentTRegister - tRegs;
        currentSRegister = currentSRegister - sRegs;
        code += "# ----------- End OperationNode ------------\n";
        return( code);
    }

    /**
     * Writes code for an operations node.
     * The code is written by gathering the child nodes' answers into
     * a pair of registers, and then executing the op on those registers,
     * placing the result in the given result register.
     * @param opNode The operation node to perform.
     * @param resultRegister The register in which to put the result.
     * @return The code which executes this operation.
     */
    public String writeRealOpCode( OperationNode opNode, String resultRegister)
    {
        String code = null;

        code = "\n# Writing code for OperationNode\n";
        ExpressionNode left = opNode.getLeft();
        String leftRegister = "";
        boolean leftIsInteger = opNode.getLeft().getType() == DataType.INTEGER;
        int tRegs = 0;
        int sRegs = 0;
        int fRegs = 0;


        if(!(scope.equals("global_"))){
            if(leftIsInteger) {
                leftRegister = "$s" + currentSRegister++;
                sRegs++;
            }
            else{
                leftRegister = "$f2" + currentSRegister++;
                sRegs++;
            }
        }
        else{
            if(leftIsInteger) {

                leftRegister = "$t" + currentTRegister++;
                tRegs++;
            }
            else{
                leftRegister = "$f" + currentFRegister++;
                fRegs++;
            }
        }
        code += writeCode( left, leftRegister);

        if(leftIsInteger){
            String conversionReg = "$f";
            if(leftRegister.contains("s")){
                conversionReg += "2" + currentSRegister++;
                sRegs++;
            }
            else{
                conversionReg += currentFRegister++;
                fRegs++;
            }

            code += "mtc1 " + leftRegister + ", " + conversionReg + "\n";
            code += "cvt.d.w " + conversionReg + ", " + conversionReg + "\n";
            leftRegister = conversionReg;
        }

        ExpressionNode right = opNode.getRight();
        String rightRegister = "";
        boolean rightIsInteger = opNode.getRight().getType() == DataType.INTEGER;

        if(!(scope.equals("global_"))){
            if(rightIsInteger){
                rightRegister = "$s" + currentSRegister++;
                sRegs++;
                //System.out.println("T register increased! Value: " + currentTRegister);
            }
            else{
                rightRegister = "$f2" + currentSRegister++;
                sRegs++;
            }
        }
        else{
            if(rightIsInteger){
                rightRegister = "$t" + currentTRegister++;
                tRegs++;
                // System.out.println("T register increased! Value: " + currentTRegister);
            }
            else{
                rightRegister = "$f" + currentFRegister++;
                tRegs++;

            }
        }
        code += writeCode( right, rightRegister);

        if(rightIsInteger){
            String conversionReg = "$f";
            if(rightRegister.contains("s")){
                conversionReg += "2" + currentSRegister++;
                sRegs++;
            }
            else{
                conversionReg += currentFRegister++;
                fRegs++;
            }

            code += "mtc1 " + rightRegister + ", " + conversionReg + "\n";
            code += "cvt.d.w " + conversionReg + ", " + conversionReg + "\n";
            rightRegister = conversionReg;
        }

        TokenType kindOfOp = opNode.getOperation();
        if( kindOfOp == TokenType.PLUS)
        {
            // add resultregister, left, right
            code += "add.d    " + resultRegister + ",   " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if( kindOfOp == TokenType.MINUS)
        {
            // subtract resultregister, left, right
            code += "sub.d    " + resultRegister + ",   " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if( kindOfOp == TokenType.MULTIPLY)
        {
            code += "mul.d   " + leftRegister + ",   " + rightRegister + "\n";
            code += "mflo   " + resultRegister + "\n";
        }
        if(kindOfOp == TokenType.DIVIDE){
            code += "div   " + leftRegister + ",   " + rightRegister + "\n";
            code += "mflo   " + resultRegister + "\n";
        }
        if(kindOfOp == TokenType.MOD){
            code += "div   " + leftRegister + ",   " + rightRegister + "\n";
            code += "mfhi   " + resultRegister + "\n";
        }

        if(kindOfOp == TokenType.OR){
            code += "or    " + resultRegister + ",   " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if(kindOfOp == TokenType.AND){
            code += "and    " + resultRegister + ",   " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if(kindOfOp == TokenType.LESS){
            code += "c.lt.d   " + resultRegister + ",    " + leftRegister +
                    ",   " + rightRegister + "\n";
        }
        if(kindOfOp == TokenType.LESSEQ){
            code += "c.lt.d  " + resultRegister + ",    " + leftRegister +
                    ",   " + rightRegister + "\n";
            code += "bclf     " + leftRegister +
                    ",   " + rightRegister + ", Skip" + currentSkip + ":";
            code += "add     " + resultRegister + ",    $zero,    1";
            code += "Skip" + currentSkip + ":\" \n ";
        }
        if(kindOfOp == TokenType.GREAT){
            code += "c.lt.d   " + resultRegister + ",    " + rightRegister+
                    ",   " + leftRegister + "\n";
        }
        if(kindOfOp == TokenType.GREATEQ){
            code += "c.lt.d    " + resultRegister + ",    " + rightRegister +
                    ",   " + leftRegister + "\n";
            code += "bclf     " + leftRegister +
                    ",   " + rightRegister + ", Skip" + currentSkip + "\n";
            code += "add     " + resultRegister + ",    $zero,    1\n";
            code += "Skip" + currentSkip + ": \n ";
        }
        if(kindOfOp == TokenType.EQUALS){
            code += "add    " + resultRegister + ",    $zero,   $zero\n";
            code += "bclf    " + leftRegister + ",   " + rightRegister + ",   Skip" + currentSkip + "\n";
            code += "add     " + resultRegister + ",    $zero,    1\n";
            code += "Skip" + currentSkip + ": \n ";
        }
        currentTRegister = currentTRegister - tRegs;
        currentSRegister = currentSRegister - sRegs;
        code += "# ----------- End OperationNode ------------\n";
        return( code);
    }


    /**
     * Writes code for a value node.
     * The code is written by executing an add immediate with the value
     * into the destination register.
     * Writes code that looks like  addi $reg, $zero, value
     * @param valNode The node containing the value.
     * @param resultRegister The register in which to put the value.
     * @return The code which executes this value node.
     */
    public String writeCode(ValueNode valNode, String resultRegister)
    {
        String value = valNode.getAttribute();
        String code = "\n# Writing Code for ValueNode\n";
        if(valNode.getType() == DataType.INTEGER)
            code += "addi   " + resultRegister + ",   $zero, " + value + "\n";
        else
            code += "addi  " + resultRegister + ",   $zero, " + value + "\n";

        code += "# ----------- End ValueNode ------------\n";
        return( code);
    }

    /**
     * Writes code for a variable node.
     * The code loads the value of the variable that has been declared earlier into
     * the result register.
     * @param varNode The variable node.
     * @param resultRegister The register to place our result
     * @return A String of assembly code
     */
    public String writeCode(VariableNode varNode, String resultRegister){
        scope = SymbolTable.st.getScope(varNode.getName()) + "_";
        String var = scope + varNode.getName();
        String code = "\n# Writing Code for VariableNode\n";

        if(varNode.getType() == DataType.INTEGER){
            code += "lw " + resultRegister + ", " + var + "\n";

        }
        else{

            code += "lwc1 " + resultRegister + ", " + var + "\n";
        }

        code += "# ----------- End VariableNode ------------\n";
        return code;
    }

    /**
     * Writes the code for an assignment statement node.
     * The function evaluates the expression and assigns it to the variable
     * @param node The assignment node
     * @return A String of assembly code
     */
    public String writeCode(AssignmentStatementNode node){
        StringBuilder code = new StringBuilder();
        code.append("\n# Writing Code for AssignmentStatementNode\n");
        String leftRegister = "";
        scope = SymbolTable.st.getScope(node.getLvalue().getName()) + "_";
        boolean isInteger = node.getLvalue().getType() == DataType.INTEGER;
        int tRegs = 0;
        int sRegs = 0;
        int fRegs = 0;
        if(!(scope.equals("global_"))){
            if(isInteger) {
                leftRegister = "$s" + currentSRegister++;
                sRegs++;
            }
            else{
                leftRegister = "$f2" + currentSRegister++;
                sRegs++;
            }
        }
        else{
            if(isInteger) {

                leftRegister = "$t" + currentTRegister++;
                tRegs++;
            }
            else{
                leftRegister = "$f" + currentFRegister++;
                fRegs++;
            }
        }

        String rightRegister = "";
        if(node.getExpression().getType() == DataType.INTEGER) {
            rightRegister = "$t" + currentTRegister++;
        }
        else
            rightRegister = "$f" + currentFRegister++;




        String nodeCode =  writeCode(node.getExpression(), leftRegister);
        code.append(nodeCode);

        //Uncommented below two lines.
        nodeCode = writeCode(node.getLvalue(), rightRegister);
        code.append(nodeCode);
        if(node.getExpression().getType() == DataType.INTEGER){
            String conversionReg = "$f";

            conversionReg += currentFRegister++;
            fRegs++;

            code.append("mtc1 " + rightRegister + ", " + conversionReg + "\n");
            code.append("cvt.d.w " + conversionReg + ", " + conversionReg + "\n");
            rightRegister = conversionReg;
        }
        //
        scope = SymbolTable.st.getScope(node.getLvalue().getName()) + "_";
        code.append("sw " + leftRegister + ", " + scope + node.getLvalue().getName() + "\n");
        currentTRegister--;
        currentTRegister = currentTRegister - tRegs;
        currentSRegister = currentSRegister - sRegs;
        currentFRegister = currentFRegister - fRegs;
        code.append("# ----------- End AssignmentStatementNode ------------\n");
        return code.toString();
    }

    /**
     * Function to write code for a FunctionCallNode.
     * @param node The function call node to write.
     * @param resultRegister The register to put the result into.
     * @return A String of assembly code.
     */
    public String writeCode(FunctionCallNode node, String resultRegister){
        StringBuilder code = new StringBuilder();
        code.append("\n# Writing Code for FunctionCallNode\n");
        boolean argsCheck = checkArgs(SymbolTable.st.getArgs(node.getName()).size(), node.getArguments().size());
        //String resultRegister = "$t" + currentTRegister++;
        if(!argsCheck)
            System.out.println("Error when calling " + node.getName() + ": expected " +
                    SymbolTable.st.getArgs(node.getName()).size() + "arguments, received " +
                    node.getArguments().size());
        int argNumber = 0;
        for(ExpressionNode exp : node.getArguments()){
            String argRegister = "$a" + argNumber++;
            code.append(writeCode(exp, argRegister));
        }
        code.append("jal " + node.getName() + "\n");
        code.append("move " + resultRegister + " $v0\n");
        return code.toString();
    }

    /**
     * Function to write code for a ProcedureCallNode.
     * @param node The procedure call node to write.
     * @return A String of assembly code.
     */
    public String writeCode(ProcedureCallNode node){
        StringBuilder code = new StringBuilder();
        code.append("\n# Writing Code for ProcedureCallNode\n");
        boolean argsCheck = checkArgs(SymbolTable.st.getArgs(node.getName()).size(), node.getArguments().size());
        //String resultRegister = "$t" + currentTRegister++;
        if(!argsCheck)
            System.out.println("Error when calling " + node.getName() + ": expected " +
                    SymbolTable.st.getArgs(node.getName()).size() + "arguments, received " +
                    node.getArguments().size());
        int argNumber = 0;
        for(ExpressionNode exp : node.getArguments()){
            String argRegister = "$a" + argNumber++;
            code.append(writeCode(exp, argRegister));
        }
        code.append("jal " + node.getName() + "\n");

        code.append("# ----------- End ProcedureCallNode ------------\n");
        return code.toString();
    }

    /**
     * Function used to check that the arguments given match the arguments specified by the function
     * @param stArgs The number of arguments in the symbol table
     * @param nodeArgs The number of arguments held by the node
     * @return True if the arguments are equal, and false if not.
     */
    public boolean checkArgs(int stArgs, int nodeArgs){
        if(stArgs == nodeArgs)
            return true;
        else
            return false;
    }

    /**
     * Writes the code for an if statement node
     * @param node The if statement node to write
     * @return A String of assembly code
     */
    public String writeCode(IfStatementNode node){
        int currentIf = this.currentIf++;
        StringBuilder code = new StringBuilder();
        code.append("\n# Writing Code for IfStatementNode\n");
        String nodeCode = null;
        String resultRegister = "";
        int tRegs = 0, fRegs = 0;
        boolean isInteger = node.getTest().getType() == DataType.INTEGER;
        if(isInteger)
            resultRegister = "$t" + currentTRegister++;
        else
            resultRegister = "$f" + currentFRegister++;

        nodeCode = writeCode(node.getTest(), resultRegister);
        code.append(nodeCode);
        code.append("beqz   " + resultRegister + ", Else" + currentIf + "\n");
        code.append("Then" + currentIf + ":\n");
        nodeCode = writeCode(node.getThenStatement());
        code.append(nodeCode);
        code.append("j EndIf" + currentIf + "\n");
        code.append("Else" + currentIf + ":\n");
        nodeCode = writeCode(node.getElseStatement());
        code.append(nodeCode);
        code.append("EndIf" + currentIf + ":\n");
        code.append("\n");
        currentTRegister -= tRegs;
        currentFRegister -= fRegs;

        code.append("# ----------- End IfStatementNode ------------\n");
        return code.toString();
    }

    /**
     * Writes the code for a while statement node
     * @param node The while statement node to write
     * @return A String of assembly code
     */
    public String writeCode(WhileStatementNode node){
        int currentLoop = this.currentLoop++;
        StringBuilder code = new StringBuilder();
        code.append("\n# Writing Code for WhileStatementNode\n");
        String resultRegister = "";
        int tRegs = 0, fRegs = 0;
        boolean isInteger = node.getTest().getType() == DataType.INTEGER;
        if(isInteger)
            resultRegister = "$t" + currentTRegister++;
        else
            resultRegister = "$f" + currentFRegister++;

        code.append("loop" + currentLoop + ":\n");
        String nodeCode = writeCode(node.getTest(), resultRegister);
        code.append(nodeCode);
        code.append("beqz   " + resultRegister + ", EndLoop" + currentLoop + "\n");
        nodeCode = writeCode(node.getDoStatement());
        code.append(nodeCode);
        code.append("b loop" + currentLoop + "\n");
        code.append("EndLoop" + currentLoop + ":\n");
        currentTRegister = currentTRegister - tRegs;
        currentFRegister = currentFRegister - fRegs;

        code.append("# ----------- End WhileStatementNode ------------\n");
        return code.toString();
    }
    
    /**
     * Writes the code for a return statement node, although I'm not sure how to yet
     * @param node
     * @return
     */
    public String writeCode(ReturnStatementNode node){
        String nodeCode = null;
        nodeCode = writeCode(node.getExpr(), "$v0");
        String code = "\n# Writing Code for ReturnStatementNode\n";
        code += nodeCode;
        //currentTRegister--;
        return code;
    }

    /**
     * Writes code for a read statement node
     * @param node
     * @return
     */
    public String writeCode(ReadStatementNode node){
        StringBuilder code = new StringBuilder();

        // Option for Integers
        code.append("\n# Writing code for ReadStatementNode\n");
        DataType type = SymbolTable.st.getType(node.getIdent());

        if(type == DataType.INTEGER)
            code.append("li $v0, 5\n");
        else
            code.append("li $v0, 6\n");

        code.append("syscall");
        code.append("sw $v0, " + node.getIdent() + "\n");

        code.append("# ----------- End ReadStatementNode ------------\n");
        return code.toString();
    }

    /**
     * Writes code for a write statement node.
     * The code is written by executing a load immediate into the s0 register
     * from the result of the write statement's expression. This value is
     * then stored in answer and a syscall is performed.
     *
     * @param node
     * @return
     */
    public String writeCode(WriteStatementNode node){
        StringBuilder code = new StringBuilder();
        code.append("\n# Writing Code for WriteStatementNode\n");
        String resultRegister = "";
        int tRegs = 0, fRegs = 0;
        boolean isInteger = node.getExpr().getType() == DataType.INTEGER;
        if(isInteger)
         resultRegister = "$t" + currentTRegister++;
        else
         resultRegister = "$f" + currentFRegister++;

        String nodeCode = writeCode(node.getExpr(), resultRegister);
        code.append(nodeCode);
        code.append("li $v0, 1 \n");
        code.append("add $a0, " + resultRegister + ", $zero\n");
        code.append("syscall\n");

/* Prints line feed, reference:
   https://stackoverflow.com/questions/9875468/printing-newline-in-mips */
        code.append("# Prints a line feed\n");
        code.append("addi $a0, $0, 0xA #ascii code for LF \n");
        code.append("addi $v0, $0, 0xB #syscall 11 prints lower 8 bits of $a0\n");
        code.append("syscall\n");
        currentTRegister -= tRegs;
        currentFRegister -= fRegs;
        code.append("# ----------- End WriteStatementNode ------------\n");
        return code.toString();
   }
}