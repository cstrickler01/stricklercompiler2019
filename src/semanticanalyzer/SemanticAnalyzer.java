package semanticanalyzer;

import symboltable.DataType;
import symboltable.SymbolTable;
import syntaxtree.*;

import java.util.ArrayList;

/**
 * This class is used to ensure that the semantics of our compiler are correct.
 * @author Chase Strickler
 */
public class SemanticAnalyzer {
    private ProgramNode program;

    public SemanticAnalyzer(ProgramNode pn){
        this.program = pn;
    }

    /**
     * Function to assign types to each of the variables.
     * @param sn are the statement nodes in our program.
     * @return the program node.
     */
    public void assignTypes(StatementNode sn) {
        if(sn instanceof CompoundStatementNode){
            ArrayList<StatementNode> statementNodes = ((CompoundStatementNode) sn).getStatements();
            for(StatementNode snode : statementNodes){
                assignTypes(snode);
            }
        }
        else if(sn instanceof AssignmentStatementNode){
            assignTypes(((AssignmentStatementNode) sn).getLvalue());
            assignTypes(((AssignmentStatementNode) sn).getExpression());
        }
        else if(sn instanceof IfStatementNode){
            assignTypes(((IfStatementNode) sn).getTest());
            assignTypes(((IfStatementNode) sn).getElseStatement());
            assignTypes(((IfStatementNode) sn).getThenStatement());
        }
        else if(sn instanceof WhileStatementNode){
            assignTypes(((WhileStatementNode) sn).getTest());
            assignTypes(((WhileStatementNode) sn).getDoStatement());
        }
        else if(sn instanceof ReadStatementNode){
            //Not sure what to do here
        }
        else if(sn instanceof ReturnStatementNode){
            assignTypes(((ReturnStatementNode) sn).getExpr());
        }
        else if(sn instanceof WriteStatementNode){
            assignTypes(((WriteStatementNode) sn).getExpr());
        }
    }

    /**
     * Function to assign types to each of the variables.
     * @param exp is the ExpressionNode being examined
     * @return the program node.
     */
    public void assignTypes(ExpressionNode exp) {
        if(exp instanceof OperationNode) {
            assignTypes(((OperationNode) exp).getLeft());
            assignTypes(((OperationNode) exp).getRight());
            if((((OperationNode) exp).getLeft().getType() == DataType.INTEGER)
                    && (((OperationNode) exp).getRight().getType() == DataType.INTEGER)){
                exp.setType(DataType.INTEGER);
            }
            else{
                exp.setType(DataType.REAL);
            }
        }
        else if(exp instanceof ValueNode) {
            if(isReal(exp)){
                exp.setType(DataType.REAL);
            }
            else{
                exp.setType(DataType.INTEGER);
            }
        }
        else if(exp instanceof VariableNode) {
           // System.out.println("Assigning type to: " + ((VariableNode) exp).getName());
            DataType type = SymbolTable.st.getType(((VariableNode) exp).getName());
            exp.setType(type);
        }
        else if(exp instanceof FunctionCallNode) {
            DataType type = SymbolTable.st.getType(((FunctionCallNode) exp).getName());
            exp.setType(type);
        }
    }

    /**
     * Checks if an expression node is real or not.
     * @param exp is the node to check if it is real. It should be an instance of ValueNode when passed
     * @return a boolean value answering whether the node is real or not.
     */
    public boolean isReal(ExpressionNode exp){
        if(((ValueNode) exp).getAttribute().contains(".")
                || ((ValueNode) exp).getAttribute().contains("e")
                || ((ValueNode) exp).getAttribute().contains("E")){
            return true;
        }
        return false;
    }

    /**
     * Function to check types across assignments.
     * @param sn is a given statementNode.
     * @return the program node.
     */
    public boolean checkTypes(StatementNode sn) {
        if(sn instanceof CompoundStatementNode){
            ArrayList<StatementNode> statementNodes = ((CompoundStatementNode) sn).getStatements();
            for(StatementNode snode : statementNodes){
                checkTypes(snode);
            }
        }
        else if(sn instanceof AssignmentStatementNode){
            if(((AssignmentStatementNode) sn).getLvalue().getType() == DataType.REAL){
                //do nothing, as this is correct
            }
            else if( (((AssignmentStatementNode) sn).getLvalue().getType() == DataType.INTEGER)
                    && (((AssignmentStatementNode) sn).getExpression().getType() == DataType.INTEGER)){
                //do nothing, as this is correct
            }
            else{
                System.out.println("Error, " + ((AssignmentStatementNode) sn).getLvalue() + " is of type "
                + ((AssignmentStatementNode) sn).getLvalue().getType() + " and is trying to be assigned "
                + ((AssignmentStatementNode) sn).getExpression() + " of type "
                + ((AssignmentStatementNode) sn).getExpression().getType() + ".\n");
                return false;
            }
        }
        else if(sn instanceof IfStatementNode){
        //    checkTypes(((IfStatementNode) sn).getTest());
            checkTypes(((IfStatementNode) sn).getElseStatement());
            checkTypes(((IfStatementNode) sn).getThenStatement());
        }
        else if(sn instanceof WhileStatementNode){
        //    checkTypes(((WhileStatementNode) sn).getTest());
            checkTypes(((WhileStatementNode) sn).getDoStatement());
        }
//        else if(sn instanceof ProcedureCallNode){
//            ArrayList<ExpressionNode> nodes = ((ProcedureCallNode) sn).getArguments();
//            for(ExpressionNode node : nodes){
//                checkTypes(node);
//            }
//            checkTypes(((ProcedureCallNode) sn).getArguments())
//        }
        return true;
    }

    /**
     * Checks statements for variables to ensure they are declared before use.
     * @param sn is the statement node to check
     * @return true if it has been declared, false otherwise.
     */
    public boolean checkVariableDecs(StatementNode sn){
        boolean bool = true;
        if(sn instanceof CompoundStatementNode){
            ArrayList<StatementNode> statementNodes = ((CompoundStatementNode) sn).getStatements();
            for(StatementNode snode : statementNodes){
                bool = bool && checkVariableDecs(snode);
            }
        }
        else if(sn instanceof AssignmentStatementNode){
            bool = bool && checkVariableDecs(((AssignmentStatementNode) sn).getLvalue());
            bool = bool && checkVariableDecs(((AssignmentStatementNode) sn).getExpression());
        }
        else if(sn instanceof IfStatementNode){
            bool = bool && checkVariableDecs(((IfStatementNode) sn).getTest());
            bool = bool && checkVariableDecs(((IfStatementNode) sn).getElseStatement());
            bool = bool && checkVariableDecs(((IfStatementNode) sn).getThenStatement());
        }
        else if(sn instanceof WhileStatementNode){
            bool = bool && checkVariableDecs(((WhileStatementNode) sn).getTest());
            bool = bool && checkVariableDecs(((WhileStatementNode) sn).getDoStatement());
        }
        else if(sn instanceof ReadStatementNode){
            //Not sure what to do here
        }
        else if(sn instanceof ReturnStatementNode){
            bool = bool && checkVariableDecs(((ReturnStatementNode) sn).getExpr());
        }
        else if(sn instanceof WriteStatementNode){
            bool = bool && checkVariableDecs(((WriteStatementNode) sn).getExpr());
        }
        else if(sn instanceof ProcedureCallNode){
            for(ExpressionNode node : ((ProcedureCallNode) sn).getArguments()){
                checkVariableDecs(node);
            }
        }
        return bool;
    }

    /**
     * Checks that variables in expressions are declared before use.
     * @param exp is the expression node to check
     * @return true if it has been declared, false otherwise.
     */
    public boolean checkVariableDecs(ExpressionNode exp){
        if(exp instanceof OperationNode) {
            checkVariableDecs(((OperationNode) exp).getLeft());
            checkVariableDecs(((OperationNode) exp).getRight());
        }
        else if(exp instanceof ValueNode) {

        }
        else if(exp instanceof VariableNode) {
            try {
                SymbolTable.st.getKind(((VariableNode) exp).getName());
            }
            catch (Exception e){
                System.out.println("Variable " + ((VariableNode) exp).getName() + " has not been declared!");
                return false;
            }
        }
        else if(exp instanceof FunctionCallNode){
            for(ExpressionNode node : ((FunctionCallNode) exp).getArguments()){
                checkVariableDecs(node);
            }
        }
        return true;
    }

    /**
     * This is the main function to determine if the pascal is semantically correct.
     * @return true if it is correct, and false otherwise.
     */
    public boolean isCorrectPascal(){
        assignTypes(this.program.getMain());
        boolean typeCheck = checkTypes(this.program.getMain());
        boolean checkVars = checkVariableDecs(this.program.getMain());

        for(SubProgramNode subprog : this.program.getFunctions().getSubProgramNodes()){
            assignTypes(subprog.getMain());
            typeCheck = checkTypes(subprog.getMain());
            checkVars = checkVariableDecs(subprog.getMain());
        }

        return (typeCheck && checkVars);
    }
}