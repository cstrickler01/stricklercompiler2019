/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symboltable;

/**
 * This enumerates the data types that a symbol can be.
 * @author Chase
 */
public enum DataType {
    INTEGER,
    REAL
}
