/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symboltable;

/**
 * This is the enumeration of the different kind of symbols that can appear.
 * @author Chase
 */
public enum KindEnum {
    VARIABLENAME,
    PROGRAMNAME,
    FUNCTIONNAME,
    PROCEDURENAME,
    ARRAYNAME,
}
