package symboltable;

import syntaxtree.VariableNode;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This is the Symbol Table for the micro-Pascal compiler.
 * @author Chase Strickler
 */
public class SymbolTable {
    // Created singleton instance of the symbol table.
    // Refer to: https://en.wikipedia.org/wiki/Singleton_pattern
    public static SymbolTable st = new SymbolTable();

    HashMap<String, Symbol> symbols;


    public SymbolTable() {
        this.symbols = new HashMap();
    }

    /**
     * This function returns the kind of type KindEnum of a symbol
     * @param ident is the identifier
     * @return the KindEnum type of the identifier
     */
    public KindEnum getKind(String ident){
        Symbol sym = SymbolTable.st.symbols.get(ident);
        return sym.kind;
    }

    /**
     * This function returns the scope of a symbol.
     * @param ident is the identifier
     * @return A String stating the scope of the identifier
     */
    public String getScope(String ident){
        Symbol sym = SymbolTable.st.symbols.get(ident);
        return sym.scope;
    }

    /**
     * Returns the symbol table
     * @return symbol table
     */
    public HashMap<String, Symbol> getSymbolTable(){

        return SymbolTable.st.symbols;
    }

    /**
     * This function returns the type of the symbol back to the user
     * @param ident the identifier
     * @return the data type of the symbol
     */
    public DataType getType(String ident){
        Symbol sym = SymbolTable.st.symbols.get(ident);
        return sym.type;
    }

    public ArrayList<VariableNode> getArgs(String ident){
        Symbol sym = SymbolTable.st.symbols.get(ident);
        return sym.args;
    }

    /**
     * This is the function to add a symbol to the table. If the symbol is a 
     * variable name, it also adds the data type
     * @param ident the identifier
     * @param k the Kind of identifier
     * @param t
     */
    public void addSymbol(String ident, KindEnum k, DataType t, String scope){
        SymbolTable.st.symbols.put(ident, new Symbol(ident, k, t, scope));
    }

    /**
     * This is the function to add a symbol to the table that is a subprogram.
     * @param ident the identifier
     * @param k the kind of identifier
     * @param t the DataType of identifier
     * @param args the arguments of the identifier
     */
    public void addSymbol(String ident, KindEnum k, DataType t, ArrayList<VariableNode> args){
        SymbolTable.st.symbols.put(ident, new Symbol(ident, k, t, args));
    }

    /**
     * This is the function to add a symbol to the table that 
     * doesn't have a data type
     * @param ident the lexeme
     * @param k the Kind of identifier
     */
    public void addSymbol(String ident, KindEnum k){

        SymbolTable.st.symbols.put(ident, new Symbol(ident, k));
    }

    /**
     * Symbol class holds the name, kind, and type (if any) for a symbol
     */
    private class Symbol {
        ArrayList<VariableNode> args;
        String name;
        String scope = null;
        KindEnum kind;
        DataType type = null; // Set to null by default, not actually sure if
        // defaulting it works in Java


        /**
         * Constructor with two parameters
         * @param str is the lexeme
         * @param k is the kind of the identifier
         */
        Symbol(String str, KindEnum k){
            this.name = str;
            this.kind = k;
        }

        /**
         * Constructor with three parameters
         * @param str is the lexeme
         * @param k is the kind of identifier
         * @param t is the data type of the identifier (real or integer)
         * @param scope is the scope of the identifier
         */
        Symbol(String str, KindEnum k, DataType t, String scope){
            this.name = str;
            this.kind = k;
            this.type = t;
            this.scope = scope;
        }

        /**
         * Constructor with three parameters, used to create subprogram symbols
         * @param str is the lexeme
         * @param k is the kind of identifier
         * @param t is the data type of the identifier (real or integer)
         * @param args is the list of arguments of the node.
         */
        Symbol(String str, KindEnum k, DataType t, ArrayList<VariableNode> args){
            this.name = str;
            this.kind = k;
            this.type = t;
            this.args = args;
        }

        /**
         *
         * @return
         */
        @Override
        public String toString(){
            StringBuilder sb = new StringBuilder();

            sb.append("ID: ");
            sb.append(this.name);
            sb.append("\t Kind: ");
            sb.append(this.kind);
            sb.append("\t Type: ");
            sb.append(this.type + "\n");
            sb.append("\t Scope: ");
            sb.append(this.scope + "\n");
            //sb.append("\n");

            String str = sb.toString();
            return str;
        }

    }
}
 