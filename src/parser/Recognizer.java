package parser;



import scanner.MyScanner;
import scanner.Token;
import scanner.TokenType;
import symboltable.DataType;
import symboltable.KindEnum;
import symboltable.SymbolTable;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The parser recognizes whether an input string of tokens
 * is an expression.
 * To use a parser, create an instance pointing at a file,
 * and then call the top-level function, <code>exp()</code>.
 * If the functions returns without an error, the file
 * contains an acceptable expression.
 * @author Chase Strickler
 */
public class Recognizer {

    /* Instance Variables */

    private Token lookahead;

    MyScanner scanner;
    //SymbolTable st;

    /* Constructor(s) */

    public Recognizer( String text, boolean isFilename) {
        if(isFilename == true) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(text);
            } catch (FileNotFoundException ex) {
                error( "No file");
            }
            InputStreamReader isr = new InputStreamReader( fis);
            scanner = new MyScanner( isr);

            }
        else {
            scanner = new MyScanner( new StringReader( text));
        }
        try {
            lookahead = scanner.nextToken();
        } catch (IOException ex) {
            error( "Scan error");
        }

    }

   /* Begin Grammar Methods */


     /**
     * Executes the rule for the program non-terminal symbol in
     * the expression grammar
     */
    public void program(){
        match(TokenType.PROGRAM);
        // Symbol table integration to add the program name to the table
        SymbolTable.st.addSymbol(this.lookahead.lexeme, KindEnum.PROGRAMNAME);
       // String ident = this.lookahead.lexeme;
        match(TokenType.ID);        
        match(TokenType.SEMICOLON);
        declarations("global");
        subprogramDeclarations();
        compoundStatement();
        match(TokenType.PERIOD);
    }

    /**
     * Executes the rule for the identifier list non-terminal symbol in
     * the expression grammar
     * @return an array list of all identifiers
     */
    public List<String> identifierList(){
        List<String> lexemes = new ArrayList<>();
        lexemes.add(this.lookahead.lexeme);
        match(TokenType.ID);
        while(this.lookahead.type == TokenType.COMMA){
            match(TokenType.COMMA);
            lexemes.add(this.lookahead.lexeme);
            match(TokenType.ID);
          }
        
        return lexemes;
    }

    /**
     * Executes the rule for the declarations non-terminal symbol in
     * the expression grammar
     */
    public void declarations(String scope){
        if(this.lookahead.getType() == TokenType.VAR){       
            match(TokenType.VAR);
            List<String> lexemes = new ArrayList(identifierList());
            match(TokenType.COLON);
            TokenType t = type();
            match(TokenType.SEMICOLON);
            
            // This for loop adds symbols to the symbol table
            for(String lex: lexemes){ 
                if(t == TokenType.INTEGER)
                   SymbolTable.st.addSymbol(lex, KindEnum.VARIABLENAME, DataType.INTEGER, scope);
                else if(t == TokenType.ARRAY)
                   SymbolTable.st.addSymbol(lex, KindEnum.ARRAYNAME);
                else
                   SymbolTable.st.addSymbol(lex, KindEnum.VARIABLENAME, DataType.REAL, scope);
            }
            
            declarations(scope);
        }
        else{
            // lambda option
        }
    }

     /**
     * Executes the rule for the type non-terminal symbol in
     * the expression grammar
     */
    public TokenType type(){
        TokenType t = this.lookahead.type;
        if(this.lookahead.type == TokenType.ARRAY){
            match(TokenType.ARRAY);
            match(TokenType.LEFTBRKT);
            match(TokenType.NUM);
            match(TokenType.COLON);
            match(TokenType.NUM);
            match(TokenType.RIGHTBRKT);
            match(TokenType.OF);
            standardType();
        }
        else if(this.lookahead.type == TokenType.INTEGER ||
                this.lookahead.type == TokenType.REAL){
            standardType();
        }
        else{
            error("Type");
        }
        return t;
    }

    /**
     * Executes the rule for the standard_type non-terminal symbol in
     * the expression grammar
     * @return the data type
     */
    public DataType standardType(){
        DataType type = null;
        if(this.lookahead.type == TokenType.INTEGER){
            type = DataType.INTEGER;
            match(TokenType.INTEGER);
        }
        else if(this.lookahead.type == TokenType.REAL){
            type = DataType.REAL;
            match(TokenType.REAL);
        }
        else{
            error( "Standard Type");
        }
        return type;
    }

    /**
     * Executes the rule for the subprogram_declarations non-terminal symbol in
     * the expression grammar
     */
    public void subprogramDeclarations(){
        if(this.lookahead.type == TokenType.FUNCTION ||
           this.lookahead.type == TokenType.PROCEDURE){           
            subprogramDec();
            match(TokenType.SEMICOLON);
            subprogramDeclarations();
        }
        else{
            //  lambda option
        }
    }

    /**
     * Executes the rule for the subprogram_dec non-terminal symbol in
     * the expression grammar
     */
    public void subprogramDec(){
        String name = subprogramHead();
        declarations(name);
        compoundStatement();
    }

    /**
     * Executes the rule for the subprogram_head non-terminal symbol in
     * the expression grammar
     * @return the string of the identifier, I believe that this function could
     * be void
     */
    public String subprogramHead(){
        // Used to add symbols to the table
        String ident = new String();
        KindEnum kind = null;       
        
        if(this.lookahead.type == TokenType.FUNCTION){
            kind = KindEnum.FUNCTIONNAME;
            match(TokenType.FUNCTION);
            ident = this.lookahead.lexeme;
            match(TokenType.ID);
            arguments();
            match(TokenType.COLON);
            standardType();
            match(TokenType.SEMICOLON);
        }
        
        else if(this.lookahead.type == TokenType.PROCEDURE){
            kind = KindEnum.PROCEDURENAME;
            match(TokenType.PROCEDURE);
            ident = this.lookahead.lexeme;
            match(TokenType.ID);
            arguments();
            match(TokenType.SEMICOLON);
        }
        
        else{
            error("Subprogram Head");
        }
        
       SymbolTable.st.addSymbol(ident, kind);
        
        return ident;
    }

    /**
     * Executes the rule for the arguments non-terminal symbol in
     * the expression grammar
     */
    public void arguments(){
        if(this.lookahead.type == TokenType.LEFTPAR){
            match(TokenType.LEFTPAR);
            parameterList();
            match(TokenType.RIGHTPAR);
        }
        else{
            // lambda option
        }
    }

    /**
     * Executes the rule for the parameter non-terminal symbol in
     * the expression grammar
     */
    public void parameterList(){
        if(this.lookahead.type == TokenType.ID){
            identifierList();
            match(TokenType.COLON);
            type();
            if( this.lookahead.type == TokenType.SEMICOLON){
                match(TokenType.SEMICOLON);
                parameterList();
            }
        }
        else{
            error("Parameter List");
        }
    }

    /**
     * Executes the rule for the compound_statement non-terminal symbol in
     * the expression grammar
     */
    public void compoundStatement(){
        match(TokenType.BEGIN);
        optionalStatements();
        match(TokenType.END);
    }

    /**
     * Executes the rule for the optional_statements non-terminal symbol in
     * the expression grammar
     */
    public void optionalStatements(){
        if( this.lookahead.type == TokenType.ID ||
            this.lookahead.type == TokenType.BEGIN ||
            this.lookahead.type == TokenType.IF ||
            this.lookahead.type == TokenType.WHILE ||
            this.lookahead.type == TokenType.READ ||
            this.lookahead.type == TokenType.WRITE ||
            this.lookahead.type == TokenType.RETURN){

            statementList();
        }
        else{
            // lambda option
        }
    }

    /**
     * Executes the rule for the statement_list non-terminal symbol in
     * the expression grammar
     */
    public void statementList(){
        statement();
        if( this.lookahead.type == TokenType.SEMICOLON){
            match(TokenType.SEMICOLON);
            statementList();
        }
    }

    /**
     * Executes the rule for the statement  non-terminal symbol in
     * the expression grammar
     */
    public void statement(){
        if(this.lookahead.type == TokenType.ID){
            String ident = new String();
            KindEnum kind = null;
            ident = this.lookahead.lexeme;
            kind = SymbolTable.st.getKind(ident);
            //match(TokenType.ID);
            if( kind == KindEnum.VARIABLENAME){      //Variable            
                variable();
                match(TokenType.ASSIGNOP);
                expression();
            }
            else{  //Procedure
                procedureStatement();
            }
            

        }

        else if(this.lookahead.type == TokenType.BEGIN){ // Compound Statement
            compoundStatement();
        }
        else if(this.lookahead.type == TokenType.IF){
            match(TokenType.IF);
            expression();
            match(TokenType.THEN);
            statement();
            match(TokenType.ELSE);
            statement();
        }
        else if(this.lookahead.type == TokenType.WHILE){
            match(TokenType.WHILE);
            expression();
            match(TokenType.DO);
            statement();
        }
        else if(this.lookahead.type == TokenType.READ){
            match(TokenType.READ);
            match(TokenType.LEFTPAR);
            match(TokenType.ID);
            match(TokenType.RIGHTPAR);
        }
        else if(this.lookahead.type == TokenType.WRITE){
            match(TokenType.WRITE);
            match(TokenType.LEFTPAR);
            expression();
            match(TokenType.RIGHTPAR);
        }
        else if(this.lookahead.type == TokenType.RETURN){
            match(TokenType.RETURN);
            expression();
        }
        else{
            error("Statement");
        }
    }

    /**
     * Executes the rule for the variable non-terminal symbol in
     * the expression grammar
     */
    public void variable(){
        match(TokenType.ID);
        if(this.lookahead.type == TokenType.LEFTBRKT){
            match(TokenType.LEFTBRKT);
            expression();
            match(TokenType.RIGHTBRKT);
        }
        else{
            // This case means the variable didn't use brackets
        }
    }

    /**
     * Executes the rule for the procedure_statement non-terminal symbol in
     * the expression grammar
     */
    public void procedureStatement(){
        match(TokenType.ID);
        if(this.lookahead.type == TokenType.LEFTPAR){
            match(TokenType.LEFTPAR);
            expressionList();
            match(TokenType.RIGHTPAR);
        }
        else{
            // This case means the variable didn't use parentheses
        }
    }

    /**
     * Executes the rule for the expressionList non-terminal symbol in
     * the expression grammar
     */
    public void expressionList(){
        expression();
        if(this.lookahead.type == TokenType.COMMA){
            match(TokenType.COMMA);
            expressionList();
        }
        else{
            // This case mans the list ended
        }
    }

    /**
     * Executes the rule for the expression non-terminal symbol in
     * the expression grammar
     */
    public void expression(){
        simpleExpression();
        if(isRelop(lookahead)){
            relop();
            simpleExpression();
        }
        else{
            // This case means there are no more simple expressions
        }
    }

    /**
     * Executes the rule for the simple_expression non-terminal symbol in
     * the expression grammar
     */
    public void simpleExpression(){
        if(this.lookahead.type == TokenType.ID      ||
            this.lookahead.type == TokenType.NUM    ||
            this.lookahead.type == TokenType.NOT){
            term();
            simplePart();
        }
        else if(this.lookahead.type == TokenType.PLUS ||
            this.lookahead.type == TokenType.MINUS){
            sign();
            term();
            simplePart();
        }
        else{
            error("Simple Expression");
        }
    }

    /**
     * Executes the rule for the simplePart non-terminal symbol in
     * the expression grammar
     */
    public void simplePart(){
        if(isAddop(lookahead)){
            addop();
            term();
            simplePart();
        }
        else{
            // lambda option
        }
    }

    /**
     * Executes the rule for the term non-terminal symbol in
     * the expression grammar.
     */
    public void term() {
        factor();
        termPart();
    }

    /**
     * Executes the rule for the term&part; non-terminal symbol in
     * the expression grammar.
     */
    public void termPart() {
        if( isMulop( lookahead) ) {
            mulop();
            factor();
            termPart();
        }
        else{
            // lambda option  
        }
    }

    /**
     * Executes the rule for the factor non-terminal symbol in
     * the expression grammar.
     */
    public void factor() {
        // Executed this decision as a switch instead of an
        // if-else chain. Either way is acceptable.
        switch (lookahead.getType()) {
            case LEFTPAR:
                match( TokenType.LEFTPAR);
                expression();
                match( TokenType.RIGHTPAR);
                break;
            case ID:
                match(TokenType.ID);
                if(lookahead.type == TokenType.LEFTBRKT){
                    match(TokenType.LEFTBRKT);
                    expression();
                    match(TokenType.RIGHTBRKT);
                }
                if(lookahead.type == TokenType.LEFTPAR){
                    match(TokenType.LEFTPAR);
                    expression();
                    match(TokenType.RIGHTPAR);
                }
                break;
            case NUM:
                match( TokenType.NUM);
                break;
            case NOT:
                match(TokenType.NOT);
                factor();
                break;
            default:
                error("Factor");
                break;
        }
    }

    /**
     * Executes the rule for the sign non-terminal symbol in
     * the expression grammar
     */
    public void sign(){
        if( lookahead.getType() == TokenType.PLUS) {
            match( TokenType.PLUS);
        }
        else if( lookahead.getType() == TokenType.MINUS) {
            match( TokenType.MINUS);
        }
        else {
            error( "Sign");
        }
    }
  
    /* End Grammar Methods */

    /* Begin Supplementary Methods */
    /**
     * Determines whether or not the given token is
     * an addop token.
     * @param token The token to check.
     * @return true if the token is a addop, false otherwise
     */
    private boolean isAddop( Token token) {
        boolean answer = false;
        if( token.getType() == TokenType.PLUS      ||
            token.getType() == TokenType.MINUS     ||
            token.getType() == TokenType.OR){

            answer = true;
        }
        return answer;
    }

    /**
     * Determines whether or not the given token is
     * a relop token.
     * @param token The token to check.
     * @return true if the token is a addop, false otherwise
     */
    private boolean isRelop( Token token) {
        boolean answer = false;
        if( token.getType() == TokenType.LESS         ||
                token.getType() == TokenType.GREAT    ||
                token.getType() == TokenType.LESSEQ    ||
                token.getType() == TokenType.GREATEQ    ||
                token.getType() == TokenType.EQUALS){

            answer = true;
        }
        return answer;
    }

    /**
     * Executes the rule for the relop non-terminal symbol in
     * the expression grammar.
     */
    public void relop() {
        if( lookahead.getType() == TokenType.LESS) {
            match( TokenType.LESS);
        }
        else if( lookahead.getType() == TokenType.LESSEQ) {
            match( TokenType.LESSEQ);
        }
        else if( lookahead.getType() == TokenType.GREAT) {
            match( TokenType.GREAT);
        }
        else if( lookahead.getType() == TokenType.GREATEQ) {
            match( TokenType.GREATEQ);
        }
        else if( lookahead.getType() == TokenType.EQUALS) {
            match( TokenType.EQUALS);
        }
        else {
            error( "Relop");
        }
    }

    /**
     * Executes the rule for the addop non-terminal symbol in
     * the expression grammar.
     */
    public void addop() {
        if( lookahead.getType() == TokenType.PLUS) {
            match( TokenType.PLUS);
        }
        else if( lookahead.getType() == TokenType.MINUS) {
            match( TokenType.MINUS);
        }
        else if( lookahead.getType() == TokenType.OR) {
            match( TokenType.OR);
        }
        else {
            error( "Addop");
        }
    }



    /**
     * Determines whether or not the given token is
     * a mulop token.
     * @param token The token to check.
     * @return true if the token is a mulop, false otherwise
     */
    private boolean isMulop( Token token) {
        boolean answer = false;
        if( token.getType() == TokenType.MULTIPLY   ||
            token.getType() == TokenType.DIVIDE     ||
            token.getType() == TokenType.DIV        ||
            token.getType() == TokenType.MOD        ||
            token.getType() == TokenType.AND ){

            answer = true;
        }
        return answer;
    }

    /**
     * Executes the rule for the mulop non-terminal symbol in
     * the expression grammar.
     */
    public void mulop() {
        if( lookahead.getType() == TokenType.MULTIPLY) {
            match( TokenType.MULTIPLY);
        }
        else if( lookahead.getType() == TokenType.DIVIDE) {
            match( TokenType.DIVIDE);
        }
        else if( lookahead.getType() == TokenType.DIV) {
            match( TokenType.DIV);
        }
        else if( lookahead.getType() == TokenType.MOD) {
            match( TokenType.MOD);
        }
        else if( lookahead.getType() == TokenType.AND) {
            match( TokenType.AND);
        }
        else {
            error( "Mulop");
        }
    }


    /**
     * Matches the expected token.
     * If the current token in the input stream from the scanner
     * matches the token that is expected, the current token is
     * consumed and the scanner will move on to the next token
     * in the input.
     * The null at the end of the file returned by the
     * scanner is replaced with a fake token containing no
     * type.
     * @param expected The expected token type.
     */
    public void match( TokenType expected) {
        //System.out.println("match( " + expected + ")");
        if( this.lookahead.getType() == expected) {
            try {
                this.lookahead = scanner.nextToken();
                if( this.lookahead == null) {
                    this.lookahead = new Token( "End of File", null);
                }
            } catch (IOException ex) {
                error( "Scanner exception");
            }
        }
        else {
            error("Match of " + expected + " found " + this.lookahead.getType()
                    + " instead.");
        }
    }

    /**
     * Errors out of the parser.
     * Prints an error message and then exits the program.
     * @param message The error message to print.
     */
    public void error( String message) {
        System.out.println( "Error " + message + " at line " +
                this.scanner.getLine() + " column " +
                this.scanner.getColumn());
        throw new RuntimeException(message);
    }

    /* End Supplementary Methods */
}
