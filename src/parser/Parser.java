/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;
import scanner.MyScanner;
import scanner.Token;
import scanner.TokenType;
import symboltable.DataType;
import symboltable.KindEnum;
import symboltable.SymbolTable;
import syntaxtree.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The parser recognizes whether an input string of tokens
 * is an expression.
 * To use a parser, create an instance pointing at a file,
 * and then call the top-level function, <code>exp()</code>.
 * If the functions returns without an error, the file
 * contains an acceptable expression.
 * @author Chase Strickler
 */
public class Parser {

    /* Instance Variables */

    private Token lookahead;

    MyScanner scanner;
    //SymbolTable st;

    /* Constructor(s) */

    public Parser( String text, boolean isFilename) {
        if(isFilename == true) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(text);
            } catch (FileNotFoundException ex) {
                error( "No file");
            }
            InputStreamReader isr = new InputStreamReader( fis);
            scanner = new MyScanner( isr);

            }
        else {
            scanner = new MyScanner( new StringReader( text));
        }
        try {
            lookahead = scanner.nextToken();
        } catch (IOException ex) {
            error( "Scan error");
        }
    }

   /* Begin Grammar Methods */


     /**
     * Executes the rule for the program non-terminal symbol in
     * the expression grammar
     */
    public ProgramNode program(){
        match(TokenType.PROGRAM);
        // Symbol table integration to add the program name to the table
        SymbolTable.st.addSymbol(this.lookahead.lexeme, KindEnum.PROGRAMNAME);
        String ident = this.lookahead.lexeme;
        match(TokenType.ID);        
        match(TokenType.SEMICOLON);
        DeclarationsNode decNode = declarations("global");
        SubProgramDeclarationsNode subProgDec = subprogramDeclarations();
        CompoundStatementNode cmpNode = compoundStatement();
        match(TokenType.PERIOD);

        ProgramNode prog = new ProgramNode(ident);
        prog.setVariables(decNode);
        prog.setFunctions(subProgDec);
        prog.setMain(cmpNode);
        return prog;
    }

    /**
     * Executes the rule for the identifier list non-terminal symbol in
     * the expression grammar
     * @return an array list of all identifiers
     */
    public List<String> identifierList(){
        List<String> lexemes = new ArrayList<>();
        lexemes.add(this.lookahead.lexeme);
        match(TokenType.ID);
        while(this.lookahead.type == TokenType.COMMA){
            match(TokenType.COMMA);
            lexemes.add(this.lookahead.lexeme);
            match(TokenType.ID);
          }
        return lexemes;
    }

    /**
     * Executes the rule for the declarations non-terminal symbol in
     * the expression grammar
     */
    public DeclarationsNode declarations(String scope){
        DeclarationsNode decs = new DeclarationsNode();
        if(this.lookahead.getType() == TokenType.VAR){       
            match(TokenType.VAR);
            List<String> lexemes = new ArrayList(identifierList());
            match(TokenType.COLON);
            TokenType t = type();
            match(TokenType.SEMICOLON);

            // This for loop adds symbols to the symbol table
            for(String lex: lexemes){
                VariableNode var = new VariableNode(lex);
                decs.addVariable(var);

                if(t == TokenType.INTEGER){
                    var.setType(DataType.INTEGER);
                    SymbolTable.st.addSymbol(lex, KindEnum.VARIABLENAME, DataType.INTEGER, scope);
                }
                else if(t == TokenType.ARRAY) {
                    SymbolTable.st.addSymbol(lex, KindEnum.ARRAYNAME);
                }
                else{
                    var.setType(DataType.REAL);
                    SymbolTable.st.addSymbol(lex, KindEnum.VARIABLENAME, DataType.REAL, scope);
                }
            }
            declarations(scope);
        }
        else {
            // lambda option
        }
        return decs;
    }

     /**
     * Executes the rule for the type non-terminal symbol in
     * the expression grammar
     */
    public TokenType type(){
        TokenType t = this.lookahead.type;
        if(this.lookahead.type == TokenType.ARRAY){
            match(TokenType.ARRAY);
            match(TokenType.LEFTBRKT);
            match(TokenType.NUM);
            match(TokenType.COLON);
            match(TokenType.NUM);
            match(TokenType.RIGHTBRKT);
            match(TokenType.OF);
            standardType();
        }
        else if(this.lookahead.type == TokenType.INTEGER ||
                this.lookahead.type == TokenType.REAL){
            standardType();
        }
        else{
            error("Type");
        }
        return t;
    }

    /**
     * Executes the rule for the standard_type non-terminal symbol in
     * the expression grammar
     * @return the data type
     */
    public DataType standardType(){
        DataType type = null;
        if(this.lookahead.type == TokenType.INTEGER){
            type = DataType.INTEGER;
            match(TokenType.INTEGER);
        }
        else if(this.lookahead.type == TokenType.REAL){
            type = DataType.REAL;
            match(TokenType.REAL);
        }
        else{
            error( "Standard Type");
        }
        return type;
    }

    /**
     * Executes the rule for the subprogram_declarations non-terminal symbol in
     * the expression grammar
     */
    public SubProgramDeclarationsNode subprogramDeclarations(){
        SubProgramDeclarationsNode subProgDecs = new SubProgramDeclarationsNode();
        while(this.lookahead.type == TokenType.FUNCTION ||
           this.lookahead.type == TokenType.PROCEDURE){
            subProgDecs.addSubProgramDeclaration(subprogramDec(this.lookahead.type));
            match(TokenType.SEMICOLON);
            //subprogramDeclarations();
        }

        return subProgDecs;
    }

    /**
     * Executes the rule for the subprogram_dec non-terminal symbol in
     * the expression grammar
     */
    public SubProgramNode subprogramDec(TokenType type){
        SubProgramHeadNode head = subprogramHead();
        SubProgramNode subprog = new SubProgramNode(head);
        //subprog.setType(type);
        subprog.setVariables(declarations(head.getName()));
        subprog.setMain(compoundStatement());
        return subprog;
    }

    /**
     * Executes the rule for the subprogram_head non-terminal symbol in
     * the expression grammar
     * @return the string of the identifier, I believe that this function could
     * be void
     */
    public SubProgramHeadNode subprogramHead(){
        // Used to add symbols to the table
        KindEnum kind = null;
        String ident = new String();
        SubProgramHeadNode head = new SubProgramHeadNode();
        DataType type = null;

        if(this.lookahead.type == TokenType.FUNCTION){
            kind = KindEnum.FUNCTIONNAME;
            match(TokenType.FUNCTION);
            ident = this.lookahead.lexeme;
            head.setName(ident);
            match(TokenType.ID);
            head.setArguments(arguments(ident));
            match(TokenType.COLON);
            type = standardType();
            head.setType(type);
            match(TokenType.SEMICOLON);
        }
        
        else if(this.lookahead.type == TokenType.PROCEDURE){
            kind = KindEnum.PROCEDURENAME;
            match(TokenType.PROCEDURE);
            ident = this.lookahead.lexeme;
            head.setName(ident);
            match(TokenType.ID);
            head.setArguments(arguments(ident));
            match(TokenType.SEMICOLON);
        }
        
        else{
            error("Subprogram Head");
        }
        
       SymbolTable.st.addSymbol(ident, kind, type, head.getArguments());
        
        return head;
    }

    /**
     * Executes the rule for the arguments non-terminal symbol in
     * the expression grammar
     */
    public ArrayList<VariableNode> arguments(String scope){
        ArrayList<VariableNode> args = new ArrayList<>();
        if(this.lookahead.type == TokenType.LEFTPAR){
            match(TokenType.LEFTPAR);
            args = parameterList(scope);
            match(TokenType.RIGHTPAR);
        }
        else{
            // lambda option
        }
        return args;
    }

    /**
     * Executes the rule for the compound_statement non-terminal symbol in
     * the expression grammar
     */
    public CompoundStatementNode compoundStatement(){
        CompoundStatementNode compound = new CompoundStatementNode();
        match(TokenType.BEGIN);
        List<StatementNode> nodes = optionalStatements();

        for(StatementNode node : nodes)
            compound.addStatement(node);

        match(TokenType.END);
        return compound;
    }

    /**
     * Executes the rule for the parameter non-terminal symbol in
     * the expression grammar
     */
    public ArrayList<VariableNode> parameterList(String scope){
        List<String> lexemes = new ArrayList<>();
        ArrayList<VariableNode> args = new ArrayList<>();
        TokenType type = null;
        if(this.lookahead.type == TokenType.ID){
            lexemes = identifierList();
            match(TokenType.COLON);
            type = type();
            if( this.lookahead.type == TokenType.SEMICOLON){
                match(TokenType.SEMICOLON);
                parameterList(scope);
            }
        }
        else{
            error("Parameter List");
        }

        for(String lex: lexemes){
            VariableNode var = new VariableNode(lex);
            args.add(var);

            if(type == TokenType.INTEGER){
                var.setType(DataType.INTEGER);
                SymbolTable.st.addSymbol(lex, KindEnum.VARIABLENAME, DataType.INTEGER, scope);
            }
            else if(type == TokenType.ARRAY) {
                SymbolTable.st.addSymbol(lex, KindEnum.ARRAYNAME);
            }
            else{
                var.setType(DataType.REAL);
                SymbolTable.st.addSymbol(lex, KindEnum.VARIABLENAME, DataType.REAL, scope);
            }
        }

        return args;
    }

    /**
     * Executes the rule for the optional_statements non-terminal symbol in
     * the expression grammar
     */
    public List<StatementNode> optionalStatements(){
        List<StatementNode> nodes = new ArrayList<>();
        if( this.lookahead.type == TokenType.ID ||
            this.lookahead.type == TokenType.BEGIN ||
            this.lookahead.type == TokenType.IF ||
            this.lookahead.type == TokenType.WHILE ||
            this.lookahead.type == TokenType.READ ||
            this.lookahead.type == TokenType.WRITE ||
            this.lookahead.type == TokenType.RETURN){

            nodes = statementList();
        }
        else{
            // lambda option
        }
        return nodes;
    }

    /**
     * Executes the rule for the statement_list non-terminal symbol in
     * the expression grammar
     */
    public List<StatementNode> statementList(){
        List<StatementNode> nodes = new ArrayList<>();
        nodes.add(statement());
        while( this.lookahead.type == TokenType.SEMICOLON){
            match(TokenType.SEMICOLON);
            StatementNode snode = statement();
            if(snode != null)
                nodes.add(snode);
        }
        return nodes;
    }

    /**
     * Executes the rule for the statement  non-terminal symbol in
     * the expression grammar
     */
    public StatementNode statement(){
        StatementNode sNode = null;
        if(this.lookahead.type == TokenType.ID){
            String ident = this.lookahead.lexeme;
            KindEnum kind = SymbolTable.st.getKind(ident);
            if( kind == KindEnum.VARIABLENAME){      //Variable
                sNode = new AssignmentStatementNode();
                ((AssignmentStatementNode) sNode).setLvalue(variable());
                match(TokenType.ASSIGNOP);
                ((AssignmentStatementNode) sNode).setExpression(expression());
            }
            else{  //Procedure
                sNode = procedureStatement();
            }
        }

        else if(this.lookahead.type == TokenType.BEGIN){ // Compound Statement
            sNode = compoundStatement();
        }
        else if(this.lookahead.type == TokenType.IF){
            sNode = new IfStatementNode();
            match(TokenType.IF);
            ((IfStatementNode) sNode).setTest(expression());
            match(TokenType.THEN);
            ((IfStatementNode) sNode).setThenStatement(statement());
            match(TokenType.ELSE);
            ((IfStatementNode) sNode).setElseStatement(statement());
        }
        else if(this.lookahead.type == TokenType.WHILE){
            match(TokenType.WHILE);
            sNode = new WhileStatementNode();
            ((WhileStatementNode) sNode).setTest(expression());
            match(TokenType.DO);
            ((WhileStatementNode) sNode).setDoStatement(statement());
        }
        else if(this.lookahead.type == TokenType.READ){
            sNode = new ReadStatementNode();
            match(TokenType.READ);
            match(TokenType.LEFTPAR);
            ((ReadStatementNode) sNode).setIdent(this.lookahead.lexeme);
            match(TokenType.ID);
            match(TokenType.RIGHTPAR);
        }
        else if(this.lookahead.type == TokenType.WRITE){
            sNode = new WriteStatementNode();
            match(TokenType.WRITE);
            match(TokenType.LEFTPAR);
            ((WriteStatementNode) sNode).setExpr(expression());
            match(TokenType.RIGHTPAR);
        }
        else if(this.lookahead.type == TokenType.RETURN){
            match(TokenType.RETURN);
            sNode = new ReturnStatementNode();
            ((ReturnStatementNode) sNode).setExpr(expression());
        }
        else{
            error("Statement");
        }
        return sNode;
    }

    /**
     * Executes the rule for the variable non-terminal symbol in
     * the expression grammar
     */
    public VariableNode variable(){
        VariableNode var = new VariableNode(this.lookahead.lexeme);
        match(TokenType.ID);
        if(this.lookahead.type == TokenType.LEFTBRKT){
            match(TokenType.LEFTBRKT);
            expression();
            match(TokenType.RIGHTBRKT);
        }
        else{
            // This case means the variable didn't use brackets
        }
        return var;
    }

    /**
     * Executes the rule for the procedure_statement non-terminal symbol in
     * the expression grammar
     */
    public ProcedureCallNode procedureStatement(){
        ProcedureCallNode node = new ProcedureCallNode();
        String ident = this.lookahead.lexeme;
        node.setString(ident);
        match(TokenType.ID);
        if(this.lookahead.type == TokenType.LEFTPAR){
            match(TokenType.LEFTPAR);
            node.setArguments(expressionList());
            match(TokenType.RIGHTPAR);
        }
        else{

        }
        return node;
    }

    /**
     * Executes the rule for the expressionList non-terminal symbol in
     * the expression grammar
     */
    public ArrayList<ExpressionNode> expressionList(){
        ArrayList<ExpressionNode> nodes = new ArrayList<>();
        nodes.add(expression());
        while(this.lookahead.type == TokenType.COMMA){
            match(TokenType.COMMA);
            nodes.add(expression());
           // expressionList();
        }
        return nodes;
    }

    /**
     * Executes the rule for the expression non-terminal symbol in
     * the expression grammar
     */
    public ExpressionNode expression(){
        ExpressionNode left = simpleExpression();
        if(isRelop(lookahead)){
            OperationNode opNode = new OperationNode(relop());
            opNode.setLeft(left);
            opNode.setRight(simpleExpression());
            return opNode;
        }
        return left;
    }

    /**
     * Executes the rule for the simple_expression non-terminal symbol in
     * the expression grammar
     */
    public ExpressionNode simpleExpression(){
        ExpressionNode expNode = null;
        if(this.lookahead.type == TokenType.ID      ||
            this.lookahead.type == TokenType.NUM    ||
            this.lookahead.type == TokenType.NOT){


            ExpressionNode xpress = term();
            return simplePart(xpress);
        }
        else if(this.lookahead.type == TokenType.PLUS ||
            this.lookahead.type == TokenType.MINUS){
            sign();

            ExpressionNode xpress = term();

            return simplePart(xpress);
        }
        else{
            error("Simple Expression");
        }
        return expNode;
    }

    /**
     * Executes the rule for the simplePart non-terminal symbol in
     * the expression grammar
     */
    public ExpressionNode simplePart(ExpressionNode possibleLeft){
        if( isAddop( lookahead) ) {
            OperationNode on = new OperationNode(addop());
            ExpressionNode right = term();
            on.setLeft(possibleLeft);
            on.setRight(right);
            return simplePart(on);

        }
        else{
            // lambda option
            return possibleLeft;
        }
    }

    /**
     * Executes the rule for the term non-terminal symbol in
     * the expression grammar.
     */
    public ExpressionNode term() {
        ExpressionNode expNode = null;
        ExpressionNode left = factor();
        return termPart(left);
    }

    /**
     * Executes the rule for the term&part; non-terminal symbol in
     * the expression grammar.
     */
    public ExpressionNode termPart(ExpressionNode possibleLeft) {
        if( isMulop( lookahead) ) {
            OperationNode on = new OperationNode(mulop());
            ExpressionNode right = factor();
            on.setLeft(possibleLeft);
            on.setRight(right);
            return termPart(on);

        }
        else{
            // lambda option
            return possibleLeft;
        }
    }

    /**
     * Executes the rule for the factor non-terminal symbol in
     * the expression grammar.
     */
    public ExpressionNode factor() { // Returns expression node
        // Executed this decision as a switch instead of an
        // if-else chain. Either way is acceptable.
        ExpressionNode expNode = null;
        switch (lookahead.getType()) {
            case LEFTPAR:
                match( TokenType.LEFTPAR);
                expression();
                match( TokenType.RIGHTPAR);
                break;
            case ID:
                expNode = new VariableNode(this.lookahead.lexeme);
                String ident = this.lookahead.lexeme;
                match(TokenType.ID);
                if(lookahead.type == TokenType.LEFTBRKT){
                    match(TokenType.LEFTBRKT);
                    expression();
                    match(TokenType.RIGHTBRKT);
                }
                if(lookahead.type == TokenType.LEFTPAR){
                    expNode = new FunctionCallNode();
                    ((FunctionCallNode) expNode).setString(ident);
                    match(TokenType.LEFTPAR);
                    ((FunctionCallNode) expNode).setArguments(expressionList());
                    match(TokenType.RIGHTPAR);
                }
                break;
            case NUM:
                expNode = new ValueNode(lookahead.getLexeme());
                match( TokenType.NUM);
                break;
            case NOT:
                match(TokenType.NOT);
                factor();
                break;
            default:
                error("Factor");
                break;
        }
        return expNode;
    }

    /**
     * Executes the rule for the sign non-terminal symbol in
     * the expression grammar
     */
    public TokenType sign(){
        TokenType op = lookahead.getType();
        if( lookahead.getType() == TokenType.PLUS) {
           // op = new OperationNode(lookahead.getType());
            match( TokenType.PLUS);
        }
        else if( lookahead.getType() == TokenType.MINUS) {
            //op = new OperationNode(lookahead.getType());
            match( TokenType.MINUS);
        }
        else {
            error( "Sign");
        }
        return op;
    }

    /* End Grammar Methods */

    /* Begin Supplementary Methods */
    /**
     * Determines whether or not the given token is
     * an addop token.
     * @param token The token to check.
     * @return true if the token is a addop, false otherwise
     */
    private boolean isAddop( Token token) {
        boolean answer = false;
        if( token.getType() == TokenType.PLUS      ||
            token.getType() == TokenType.MINUS     ||
            token.getType() == TokenType.OR){

            answer = true;
        }
        return answer;
    }

    /**
     * Executes the rule for the addop non-terminal symbol in
     * the expression grammar.
     */
    public TokenType addop() {
        TokenType tok = lookahead.getType();
        if( lookahead.getType() == TokenType.PLUS) {
            match( TokenType.PLUS);
        }
        else if( lookahead.getType() == TokenType.MINUS) {
            match( TokenType.MINUS);
        }
        else if( lookahead.getType() == TokenType.OR) {
            match( TokenType.OR);
        }
        else {
            error( "Addop");
        }
        return tok;
    }

    /**
     * Determines whether or not the given token is
     * a relop token.
     * @param token The token to check.
     * @return true if the token is a addop, false otherwise
     */
    private boolean isRelop( Token token) {
        boolean answer = false;
        if( token.getType() == TokenType.LESS         ||
                token.getType() == TokenType.GREAT    ||
                token.getType() == TokenType.LESSEQ    ||
                token.getType() == TokenType.GREATEQ    ||
                token.getType() == TokenType.EQUALS){

            answer = true;
        }
        return answer;
    }

    /**
     * Executes the rule for the relop non-terminal symbol in
     * the expression grammar.
     */
    public TokenType relop() {
        TokenType tok = lookahead.getType();
        if( lookahead.getType() == TokenType.LESS) {
            match( TokenType.LESS);
        }
        else if( lookahead.getType() == TokenType.LESSEQ) {
            match( TokenType.LESSEQ);
        }
        else if( lookahead.getType() == TokenType.GREAT) {
            match( TokenType.GREAT);
        }
        else if( lookahead.getType() == TokenType.GREATEQ) {
            match( TokenType.GREATEQ);
        }
        else if( lookahead.getType() == TokenType.EQUALS) {
            match( TokenType.EQUALS);
        }
        else {
            error( "Relop");
        }
        return tok;
    }

    /**
     * Determines whether or not the given token is
     * a mulop token.
     * @param token The token to check.
     * @return true if the token is a mulop, false otherwise
     */
    private boolean isMulop( Token token) {
        boolean answer = false;
        if( token.getType() == TokenType.MULTIPLY   ||
            token.getType() == TokenType.DIVIDE     ||
            token.getType() == TokenType.DIV        ||
            token.getType() == TokenType.MOD        ||
            token.getType() == TokenType.AND ){

            answer = true;
        }
        return answer;
    }

    /**
     * Executes the rule for the mulop non-terminal symbol in
     * the expression grammar.
     */
    public TokenType mulop() {
        TokenType tok = lookahead.getType();
        if( lookahead.getType() == TokenType.MULTIPLY) {
            match( TokenType.MULTIPLY);
        }
        else if( lookahead.getType() == TokenType.DIVIDE) {
            match( TokenType.DIVIDE);
        }
        else if( lookahead.getType() == TokenType.DIV) {
            match( TokenType.DIV);
        }
        else if( lookahead.getType() == TokenType.MOD) {
            match( TokenType.MOD);
        }
        else if( lookahead.getType() == TokenType.AND) {
            match( TokenType.AND);
        }
        else {
            error( "Mulop");
        }
        return tok;
    }


    /**
     * Matches the expected token.
     * If the current token in the input stream from the scanner
     * matches the token that is expected, the current token is
     * consumed and the scanner will move on to the next token
     * in the input.
     * The null at the end of the file returned by the
     * scanner is replaced with a fake token containing no
     * type.
     * @param expected The expected token type.
     */
    public void match( TokenType expected) {
        //Uncomment below line to see a list of matched tokens
        System.out.println("match(" + expected + ")");
        if( this.lookahead.getType() == expected) {
            try {
                this.lookahead = scanner.nextToken();
                if( this.lookahead == null) {
                    this.lookahead = new Token( "End of File", null);
                }
            } catch (IOException ex) {
                error( "Scanner exception");
            }
        }
        else {
            error("Match of " + expected + " found " + this.lookahead.getType()
                   + " instead.");
        }
    }

    /**
     * Errors out of the parser.
     * Prints an error message and then exits the program.
     * @param message The error message to print.
     */
    public void error( String message) {
        System.out.println( "Error " + message + " at line " +
                this.scanner.getLine() + " column " +
                this.scanner.getColumn());
        throw new RuntimeException(message);
    }

    /* End Supplementary Methods */
}