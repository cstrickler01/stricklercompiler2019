package scanner;

/**
 *  The token takes in a word as a string and a
 *  TokenType as the type. A Token object is then created.
 *  @author Chase Strickler
 */
public class Token
{
    public String lexeme;
    public TokenType type;
    public Token(String lex, TokenType tok) {
      this.lexeme = lex;
      this.type = tok;
    }
    
    public String getLexeme() { 
        return this.lexeme;
    }
    
    public TokenType getType() { 
        return this.type;
    }
}
