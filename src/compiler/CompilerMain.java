package compiler;

import codegen.CodeGeneration;
import parser.Parser;
import semanticanalyzer.SemanticAnalyzer;
import symboltable.SymbolTable;
import syntaxtree.ProgramNode;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * This class acts as the main class of the compiler, running the parser on a
 * singleton instance of the symbol table.
 * @author Chase Strickler
 */
public class CompilerMain {
    
    /**
     * This is the main class of the compiler. It reads the name of a filename
     * from the standard input stream and passes it to the parser.
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        String filename = "";
        if(args.length < 1) {
            System.out.println("No file passed! Passing default file ../../pascal.pas ...");
            filename = "./product/sample.pas";
        }
        else
            filename = args[0];
        
        Parser parser = new Parser(filename, true);
        ProgramNode actual = parser.program();
        SemanticAnalyzer analyzer = new SemanticAnalyzer(actual);
        boolean isCorrect = analyzer.isCorrectPascal();
        CodeGeneration codegen = null;
        String mipsCode = null;
        if(isCorrect) {
            codegen = new CodeGeneration(actual);
            mipsCode = codegen.writeCodeForRoot();
        }

        String actualString = actual.indentedToString(0);

        /*  formattedString is the string returned by the toString method.
            The replace calls are necessary to keep it clean */
        String formattedString = SymbolTable.st.getSymbolTable().values().toString()
                .replace("[", "")
                .replace("]", "")
                .replace(", ", "");

        FileWriter writer = new FileWriter("symbol.table");
        FileWriter tree_writer = new FileWriter("syntax.tree");
        FileWriter code_writer = new FileWriter("code.asm");

        /* Prints the symbol table to symbol.table */
        try {
            PrintWriter pw = new PrintWriter(writer);
            pw.print(formattedString);
            pw.close();
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }

        /* Prints the syntax tree to symbol.tree */
        try {
            PrintWriter pw = new PrintWriter(tree_writer);
            pw.print(actualString);

            pw.close();
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }

        /* Prints the generated code to code.asm */
        try{
            PrintWriter pw = new PrintWriter(code_writer);
            pw.print(mipsCode);
            pw.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}