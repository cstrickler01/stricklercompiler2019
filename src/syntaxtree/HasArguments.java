package syntaxtree;

import java.util.ArrayList;

public interface HasArguments {
    public ArrayList<ExpressionNode> getArguments();
}
