package syntaxtree;

import symboltable.DataType;

import java.util.ArrayList;

public class SubProgramHeadNode extends SyntaxTreeNode{
    private String name;
    private ArrayList<VariableNode> arguments;
    private DataType type;

    public void setType(DataType type) {
        this.type = type;
    }

    public DataType getType() {
        return type;
    }

    public void setName(String s){
        this.name = s;
    }

    public String getName(){
        return name;
    }

    public void setArguments(ArrayList<VariableNode> args){
        this.arguments = args;
    }

    public ArrayList<VariableNode> getArguments(){
        return arguments;
    }

    @Override
    public String indentedToString(int level) {
        return null;
    }
}
