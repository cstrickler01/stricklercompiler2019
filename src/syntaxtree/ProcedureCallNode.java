package syntaxtree;

import java.util.ArrayList;

public class ProcedureCallNode extends StatementNode implements HasArguments {
    private String name;
    private ArrayList<ExpressionNode> args;

    public String getName(){
        return name;
    }

    public void setString(String str){
        name = str;
    }

    public void setArguments(ArrayList<ExpressionNode> arguments) {
        this.args = arguments;
    }

    @Override
    public ArrayList<ExpressionNode> getArguments() {
        return args;
    }

    @Override
    public String indentedToString(int level) {
        String answer = this.indentation( level);
        answer += "Procedure Call: " + name + "\n";
        for( ExpressionNode arg : args) {
            answer += arg.indentedToString( level + 1);
        }
        return answer;
    }
}
