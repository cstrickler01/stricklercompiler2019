package syntaxtree;

public class SubProgramNode extends SyntaxTreeNode {

	private SubProgramHeadNode head;
	private DeclarationsNode variables;
	private CompoundStatementNode main;

	public SubProgramNode( SubProgramHeadNode subHead) {
		this.head = subHead;
	}

	public SubProgramHeadNode getHead(){
		return head;
	}

	public DeclarationsNode getVariables() {

		return variables;
	}


	public void setVariables(DeclarationsNode variables) {

		this.variables = variables;
	}

	public CompoundStatementNode getMain() {

		return main;
	}

	public void setMain(CompoundStatementNode main) {

		this.main = main;
	}

	/**
	 * Creates a String representation of this program node and its children.
	 * @param level The tree level at which this node resides.
	 * @return A String representing this node.
	 */
	@Override
	public String indentedToString( int level) {
		String answer = this.indentation( level);
		answer += "SubProgram: " + head.getName() + ", Type: " + head.getType() + "\n";
		answer += variables.indentedToString( level + 1);
		answer += main.indentedToString( level + 1);
		return answer;
	}
}
