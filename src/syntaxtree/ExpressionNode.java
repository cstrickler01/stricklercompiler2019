package syntaxtree;

import symboltable.DataType;

/**
 * General representation of any expression.
 * @author erik
 */
public abstract class ExpressionNode extends SyntaxTreeNode {
    private DataType type;

    public DataType getType() {
        return type;
    }

    public void setType(DataType type) {
        this.type = type;
    }
}
