package syntaxtree;

import java.util.ArrayList;

public class FunctionCallNode extends ExpressionNode implements HasArguments {
    private String name;
    private ArrayList<ExpressionNode> args;
    private ExpressionNode returnvalue;

    public String getName(){
        return name;
    }

    public void setString(String str){
        name = str;
    }

    @Override
    public ArrayList<ExpressionNode> getArguments() {
        return args;
    }

    public void setArguments(ArrayList<ExpressionNode> arguments) {
        this.args = arguments;
    }

    @Override
    public String indentedToString(int level) {
        String answer = this.indentation( level);
        answer += "Function Call: " + name + ", Type: " + getType() + "\n";
        for( ExpressionNode arg : args) {
            answer += arg.indentedToString( level + 1);
        }
        return answer;
    }
}
