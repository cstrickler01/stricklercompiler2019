
package syntaxtree;

/**
 * Represents an if statement in Pascal.
 * An if statement includes a boolean expression, and two statements.
 * @author Erik Steinmetz
 */
public class ReturnStatementNode extends StatementNode {
    private ExpressionNode expr;

    public ExpressionNode getExpr() {
        return expr;
    }

    public void setExpr(ExpressionNode test) {
        this.expr = test;
    }


    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Return\n";
        answer += this.expr.indentedToString( level + 1);
        return answer;
    }

}
